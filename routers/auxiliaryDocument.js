var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../controllers'),
    config = require('annulet-config');

var auxiliaryDocumentRouter = express.Router();

auxiliaryDocumentRouter.get('/', controllerContext.auxiliaryDocument.list, config.middleware.responseHandler);
auxiliaryDocumentRouter.get('/recent', controllerContext.auxiliaryDocument.recent, config.middleware.responseHandler);
auxiliaryDocumentRouter.get('/:file', controllerContext.auxiliaryDocument.getFile);

module.exports = exports = auxiliaryDocumentRouter;
