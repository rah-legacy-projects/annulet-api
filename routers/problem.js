var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../controllers'),
    config = require('annulet-config');

var problemRouter = express.Router();

problemRouter.post('/', controllerContext.problem.make);
problemRouter.get('/coerce',
    config.middleware.loggedIn,
    config.middleware.customerValid,
    config.middleware.paymentRequired,
    controllerContext.problem.coerce,
    config.middleware.responseHandler);
problemRouter.get('/:ticket',
    config.middleware.loggedIn,
    config.middleware.customerValid,
    config.middleware.paymentRequired,
    controllerContext.problem.get,
    config.middleware.responseHandler);

module.exports = exports = problemRouter;
