var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../../controllers'),
    config = require('annulet-config');

var alertRouter = express.Router();

alertRouter.get('/', controllerContext.admin.alert.list, config.middleware.responseHandler);
alertRouter.get('/detail/:alert', controllerContext.admin.alert.load, config.middleware.responseHandler);
alertRouter.get('/tuple/:alert', controllerContext.admin.alert.loadTuple, config.middleware.responseHandler);
alertRouter.post('/create', controllerContext.admin.alert.create, config.middleware.responseHandler);
alertRouter.post('/update', controllerContext.admin.alert.update, config.middleware.responseHandler);
alertRouter.post('/publish', controllerContext.admin.alert.publish, config.middleware.responseHandler);
alertRouter.post('/close', controllerContext.admin.alert.close, config.middleware.responseHandler);

module.exports = exports = alertRouter;
