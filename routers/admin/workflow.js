var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../../controllers'),
    config = require('annulet-config');

var workflowRouter = express.Router();

workflowRouter.get('/', controllerContext.admin.workflow.list, config.middleware.responseHandler);
workflowRouter.get('/detail/shortName/:shortName', controllerContext.admin.workflow.loadByShortName, config.middleware.responseHandler);
workflowRouter.get('/detail/:workflow', controllerContext.admin.workflow.load, config.middleware.responseHandler);
workflowRouter.post('/create', controllerContext.admin.workflow.create, config.middleware.responseHandler);
workflowRouter.post('/update', controllerContext.admin.workflow.update, config.middleware.responseHandler);
workflowRouter.post('/publish', controllerContext.admin.workflow.publish, config.middleware.responseHandler);
workflowRouter.post('/close', controllerContext.admin.workflow.close, config.middleware.responseHandler);

module.exports = exports = workflowRouter;
