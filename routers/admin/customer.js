var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../../controllers'),
    config = require('annulet-config');

var customerRouter = express.Router();

customerRouter.get('/', controllerContext.admin.customer.list, config.middleware.responseHandler);
//customerRouter.post('/:customer', controllerContext.customer.detail, config.middleware.responseHandler);
customerRouter.get('/:customer', controllerContext.admin.customer.detail, config.middleware.responseHandler);

module.exports = exports = customerRouter;
