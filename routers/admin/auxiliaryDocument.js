var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../../controllers'),
    config = require('annulet-config');

var auxiliaryDocumentRouter = express.Router();

auxiliaryDocumentRouter.get('/', controllerContext.admin.auxiliaryDocument.list, config.middleware.responseHandler);
auxiliaryDocumentRouter.get('/detail/:auxiliaryDocument', controllerContext.admin.auxiliaryDocument.load, config.middleware.responseHandler);
auxiliaryDocumentRouter.get('/tuple/:auxiliaryDocument', controllerContext.admin.auxiliaryDocument.loadTuple, config.middleware.responseHandler);
auxiliaryDocumentRouter.post('/create', controllerContext.admin.auxiliaryDocument.create, config.middleware.responseHandler);
auxiliaryDocumentRouter.post('/update', controllerContext.admin.auxiliaryDocument.update, config.middleware.responseHandler);
auxiliaryDocumentRouter.post('/publish', controllerContext.admin.auxiliaryDocument.publish, config.middleware.responseHandler);
auxiliaryDocumentRouter.post('/close', controllerContext.admin.auxiliaryDocument.close, config.middleware.responseHandler);
auxiliaryDocumentRouter.post('/upload', controllerContext.admin.auxiliaryDocument.upload, config.middleware.responseHandler);

module.exports = exports = auxiliaryDocumentRouter;
