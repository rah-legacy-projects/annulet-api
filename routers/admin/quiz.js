var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../../controllers'),
    config = require('annulet-config');

var quizRouter = express.Router();

quizRouter.get('/', controllerContext.admin.quiz.list, config.middleware.responseHandler);
quizRouter.get('/detail/shortName/:shortName', controllerContext.admin.quiz.loadByShortName, config.middleware.responseHandler);
quizRouter.get('/detail/:quiz', controllerContext.admin.quiz.load, config.middleware.responseHandler);
quizRouter.post('/create', controllerContext.admin.quiz.create, config.middleware.responseHandler);
quizRouter.post('/update', controllerContext.admin.quiz.update, config.middleware.responseHandler);
quizRouter.post('/publish', controllerContext.admin.quiz.publish, config.middleware.responseHandler);
quizRouter.post('/close', controllerContext.admin.quiz.close, config.middleware.responseHandler);

module.exports = exports = quizRouter;
