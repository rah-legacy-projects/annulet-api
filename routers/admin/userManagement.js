var express = require('express'),
    logger = require('winston'),
	config = require('annulet-config'),
    controllerContext = require('../../controllers');

var userManagementRouter = express.Router();

userManagementRouter.get('/', controllerContext.admin.userManagement.getUsers, config.middleware.responseHandler);
userManagementRouter.get('/:customerUser', controllerContext.admin.userManagement.getUser, config.middleware.responseHandler);
userManagementRouter.post('/save', controllerContext.admin.userManagement.saveUser, config.middleware.responseHandler);
userManagementRouter.post('/remove', controllerContext.admin.userManagement.removeUser, config.middleware.responseHandler);

module.exports = exports = userManagementRouter;


