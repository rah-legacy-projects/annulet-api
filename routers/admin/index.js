module.exports = exports = {
	alert: require("./alert"),
	auxiliaryDocument: require("./auxiliaryDocument"),
	customer: require("./customer"),
	operatingProcedure: require("./operatingProcedure"),
	quiz: require("./quiz"),
	training: require("./training"),
	userManagement: require("./userManagement"),
	workflow: require("./workflow"),
};
