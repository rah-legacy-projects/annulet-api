var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../../controllers'),
    config = require('annulet-config');

var operatingProcedureRouter = express.Router();

operatingProcedureRouter.get('/', controllerContext.admin.operatingProcedure.list, config.middleware.responseHandler);
operatingProcedureRouter.get('/detail/shortName/:shortName', controllerContext.admin.operatingProcedure.loadByShortName, config.middleware.responseHandler);
operatingProcedureRouter.get('/detail/:operatingProcedure', controllerContext.admin.operatingProcedure.load, config.middleware.responseHandler);
operatingProcedureRouter.post('/create', controllerContext.admin.operatingProcedure.create, config.middleware.responseHandler);
operatingProcedureRouter.post('/update', controllerContext.admin.operatingProcedure.update, config.middleware.responseHandler);
operatingProcedureRouter.post('/publish', controllerContext.admin.operatingProcedure.publish, config.middleware.responseHandler);
operatingProcedureRouter.post('/close', controllerContext.admin.operatingProcedure.close, config.middleware.responseHandler);

operatingProcedureRouter.post('/activate/:operatingProcedure', controllerContext.admin.operatingProcedure.activate, config.middleware.responseHandler);
operatingProcedureRouter.post('/deactivate/:operatingProcedure', controllerContext.admin.operatingProcedure.deactivate, config.middleware.responseHandler);


module.exports = exports = operatingProcedureRouter;
