var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../../controllers'),
    config = require('annulet-config');

var trainingRouter = express.Router();

trainingRouter.get('/', controllerContext.admin.training.list, config.middleware.responseHandler);
trainingRouter.get('/detail/shortName/:shortName', controllerContext.admin.training.loadByShortName, config.middleware.responseHandler);
trainingRouter.get('/detail/:training', controllerContext.admin.training.load, config.middleware.responseHandler);
trainingRouter.post('/create', controllerContext.admin.training.create, config.middleware.responseHandler);
trainingRouter.post('/update', controllerContext.admin.training.update, config.middleware.responseHandler);
trainingRouter.post('/publish', controllerContext.admin.training.publish, config.middleware.responseHandler);
trainingRouter.post('/close', controllerContext.admin.training.close, config.middleware.responseHandler);

module.exports = exports = trainingRouter;
