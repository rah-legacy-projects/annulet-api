var express = require('express'),
    logger = require('winston'),
	config = require('annulet-config'),
    controllerContext = require('../controllers');

var quizRouter = express.Router();

quizRouter.get('/:quiz', config.middleware.paymentRequired, controllerContext.quiz.get, config.middleware.responseHandler);
quizRouter.get('/summary/:attempt', config.middleware.paymentRequired, controllerContext.quiz.getSummary, config.middleware.responseHandler);
quizRouter.post('/answer/:questionType/:question', controllerContext.quiz.saveAnswer, config.middleware.responseHandler);
quizRouter.post('/finalize/:attempt' ,controllerContext.quiz.finalize, config.middleware.responseHandler);

module.exports = exports = quizRouter;
