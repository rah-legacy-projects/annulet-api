var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../controllers'),
    config = require('annulet-config');

var stripeHookRouter = express.Router();

stripeHookRouter.post('/', controllerContext.stripeHook.post);

module.exports = exports = stripeHookRouter;
