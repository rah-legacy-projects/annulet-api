var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../controllers'),
    config = require('annulet-config');

var complaintRouter = express.Router();

//hack: list is a post, even though it should be a get: lots of information comes up, and may exceeed GET limits
complaintRouter.post('/list', controllerContext.complaint.list, config.middleware.responseHandler);
complaintRouter.get('/recent', controllerContext.complaint.recent, config.middleware.responseHandler);
complaintRouter.get('/:complaint', controllerContext.complaint.detail, config.middleware.responseHandler);
complaintRouter.post('/:complaint/addComment', controllerContext.complaint.addComment, config.middleware.responseHandler);
complaintRouter.post('/updateComplaint', controllerContext.complaint.updateComplaint, config.middleware.responseHandler);

module.exports = exports = complaintRouter;
