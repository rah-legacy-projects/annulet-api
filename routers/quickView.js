var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../controllers'),
    config = require('annulet-config');

var quickViewRouter = express.Router();

quickViewRouter.get('/userOutstandingOPs', controllerContext.quickView.userOutstandingOPs, config.middleware.responseHandler);
quickViewRouter.get('/userOutstandingWorkflows', controllerContext.quickView.userOutstandingWorkflows, config.middleware.responseHandler);
quickViewRouter.get('/adminOutstandingOPs', controllerContext.quickView.adminOutstandingOPs, config.middleware.responseHandler);
quickViewRouter.get('/adminOutstandingWorkflows', controllerContext.quickView.adminOutstandingWorkflows, config.middleware.responseHandler);
quickViewRouter.get('/adminOPActivity', controllerContext.quickView.adminOPActivity, config.middleware.responseHandler);
quickViewRouter.get('/adminWorkflowActivity', controllerContext.quickView.adminWorkflowActivity, config.middleware.responseHandler);

module.exports = exports = quickViewRouter;
