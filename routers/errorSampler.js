var express = require('express'),
	config = require('annulet-config'),
    logger = require('winston'),
    controllerContext = require('../controllers');

var errorSamplerRouter = express.Router();

errorSamplerRouter.get('/401', controllerContext.errorSampler.get401, config.middleware.responseHandler);
errorSamplerRouter.get('/402', controllerContext.errorSampler.get402, config.middleware.responseHandler);
errorSamplerRouter.get('/403', controllerContext.errorSampler.get403, config.middleware.responseHandler);
errorSamplerRouter.get('/404', controllerContext.errorSampler.get404, config.middleware.responseHandler);
errorSamplerRouter.get('/409', controllerContext.errorSampler.get409, config.middleware.responseHandler);
errorSamplerRouter.get('/500', controllerContext.errorSampler.get500, config.middleware.responseHandler);
errorSamplerRouter.get('/501', controllerContext.errorSampler.get501, config.middleware.responseHandler);
errorSamplerRouter.get('/502', controllerContext.errorSampler.get502, config.middleware.responseHandler);
errorSamplerRouter.get('/503', controllerContext.errorSampler.get503, config.middleware.responseHandler);
errorSamplerRouter.get('/504', controllerContext.errorSampler.get504, config.middleware.responseHandler);

module.exports = exports = errorSamplerRouter;
