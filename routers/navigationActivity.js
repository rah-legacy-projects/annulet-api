var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../controllers'),
    config = require('annulet-config');

var activityRouter = express.Router();

activityRouter.post('/', controllerContext.navigationActivity.make);

module.exports = exports = activityRouter;
