var express = require('express'),
    logger = require('winston'),
    config = require('annulet-config'),
    controllerContext = require('../controllers');

var operatingProcedureRouter = express.Router();
operatingProcedureRouter.get('/list', controllerContext.operatingProcedure.list, config.middleware.responseHandler);
operatingProcedureRouter.get('/:operatingProcedure', config.middleware.paymentRequired, controllerContext.operatingProcedure.get, config.middleware.responseHandler);
operatingProcedureRouter.post('/section/:sectionType/:sectionRange', controllerContext.operatingProcedure.saveView, config.middleware.responseHandler);
operatingProcedureRouter.post('/finalize/:operatingProcedureRange', controllerContext.operatingProcedure.finalize, config.middleware.responseHandler);

module.exports = exports = operatingProcedureRouter;
