var express = require('express'),
    logger = require('winston'),
	config = require('annulet-config'),
    controllerContext = require('../controllers');

var trainingRouter = express.Router();

trainingRouter.get('/:training', config.middleware.paymentRequired, controllerContext.training.get, config.middleware.responseHandler);
trainingRouter.post('/section/:sectionType/:sectionRange', controllerContext.training.saveView, config.middleware.responseHandler);
trainingRouter.post('/finalize/:trainingRange', controllerContext.training.finalize, config.middleware.responseHandler);

module.exports = exports = trainingRouter;
