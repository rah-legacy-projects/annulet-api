var express = require('express'),
    logger = require('winston'),
	config = require('annulet-config'),
    controllerContext = require('../controllers');

var signupRouter = express.Router();

signupRouter.post('/', controllerContext.signup.createCustomer, config.middleware.responseHandler);

module.exports = exports = signupRouter;


