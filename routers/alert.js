var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../controllers'),
    config = require('annulet-config');

var alertRouter = express.Router();

alertRouter.get('/', controllerContext.alert.list, config.middleware.responseHandler);
alertRouter.post('/dismiss/:alertRange', controllerContext.alert.dismiss, config.middleware.responseHandler);

module.exports = exports = alertRouter;
