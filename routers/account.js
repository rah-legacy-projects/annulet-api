var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../controllers'),
    config = require('annulet-config');

var accountRouter = express.Router();

accountRouter.get('/', controllerContext.account.paymentDetail, config.middleware.responseHandler);
accountRouter.post('/customer', controllerContext.account.updateCustomer, config.middleware.responseHandler);
accountRouter.post('/payment', controllerContext.account.updatePayment, config.middleware.responseHandler);
accountRouter.post('/deactivate', controllerContext.account.deactivateAccount, config.middleware.responseHandler);

module.exports = exports = accountRouter;
