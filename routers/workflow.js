var express = require('express'),
    logger = require('winston'),
    config = require('annulet-config'),
    controllerContext = require('../controllers');

var workflowRouter = express.Router();
workflowRouter.get('/', controllerContext.workflow.list, function(req, res, next){
	logger.silly('about to call into response handler...');
	next();
}, config.middleware.responseHandler);
workflowRouter.get('/:workflowItem', controllerContext.workflow.get, config.middleware.responseHandler);
module.exports = exports = workflowRouter;
