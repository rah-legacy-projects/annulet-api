/**
 * Module dependencies.
 */
var express = require("express"),
    util = require("util"),
    http = require("http"),
    path = require("path"),
    fs = require('fs'),
    async = require('async'),
    _ = require('lodash'),
    mo = require('method-override'),
    cors = require('cors'),
    routers = require('./routers'),
    useragent = require('express-useragent'),
    modeler = require('annulet-models')
    .modeler,
    config = require('annulet-config'),
    logger = require('winston');

_.mixin(require('annulet-util').lodashMixins);

process.on('uncaughtException', config.middleware.uncaughtException);
var app = express();

var userMiddleware = function(req, res, next) {
    var internal = req.get('annulet-internal') || req.query['annulet_internal'];
    if (!internal) {
        if (!req.loggedInUser || !req.loggedInUser._id) {
            logger.warn('no logged in user, using customer to derive model');
            modeler.db({
                collection: 'Customer',
                query: {
                    _id: req.customer.toObjectId()
                }
            }, function(err, models) {
                req.models = models;
                return next();
            });
        }
        async.waterfall([

            function(cb) {
                modeler.db({
                    collection: 'auth.customerUser',
                    query: {
                        user: req.loggedInUser._id.toObjectId(),
                        customer: req.customer.toObjectId()
                    },
                }, function(err, models) {
                    cb(err, models, {});
                });
            },
            function(models, p, cb) {
                models.auth.CustomerUser.findOne({
                    user: req.loggedInUser._id.toObjectId(),
                    customer: req.customer.toObjectId()
                })
                    .lean()
                    .exec(function(err, customerUser) {
                        p.customerUser = customerUser;
                        p.customerUser.user = req.loggedInUser;
                        cb(err, models, p);
                    });
            }
        ], function(err, models, p) {
            req.actionCustomerUser = p.customerUser;
            req.models = models;
            next();
        });
    } else {
        if (config.configuration.paths.internalKey() == internal) {
            if (!req.models) {
                modeler.db({
                    collection:'Customer',
                    query:{
                        _id: req.customer.toObjectId()
                    }
                }, function(err, models){
                    req.models = models;
                    next();
                });
            } else {
                next();
            }
        } else {
            res.status(403)
                .end();
        }
    }
};

var middlewareChain = [
    config.middleware.loggedIn,
    config.middleware.navigationActivityTracker,
    config.middleware.customerValid,
    userMiddleware,
    config.middleware.paymentRequired
];

logger.info('starting application...');
app.set("port", process.env.PORT || config.configuration.ports.apiPort());
app.use(require('body-parser')());
app.use(mo('X-HTTP-Method-Override'));
app.use(cors());
app.use(require('errorhandler')());

app.use(useragent.express());

app.use(function(req, res, next) {
    logger.silly('url: ' + req.url);
    next();
});

logger.warn('environment: ' + process.env.ENV);

app.use('/customer', function(req, res, next) {
    logger.silly('checking customer');
    next();
}, config.middleware.loggedIn, routers.customer);
logger.silly('about to set up API');

//{{{ user usage
app.use('/workflow', middlewareChain, routers.workflow);
logger.silly('workflow API set up');
app.use('/quiz', middlewareChain, routers.quiz);
logger.silly('quiz API set up');
app.use('/training', middlewareChain, routers.training);
logger.silly('training API set up');
app.use('/operatingProcedure', middlewareChain, routers.operatingProcedure);
logger.silly('operatingProcedure API set up');
app.use('/signup', config.middleware.navigationActivityTracker, routers.signup);
logger.silly('signup API set up');
app.use('/complaint', middlewareChain, routers.complaint);
logger.silly('complaint API set up');
app.use('/errorSampler', middlewareChain, routers.errorSampler);
logger.silly('error sampler api set up');
app.use('/alert', middlewareChain, routers.alert);
logger.silly('alert api set up');
app.use('/stripeHook', config.middleware.navigationActivityTracker, routers.stripeHook);
logger.silly('stripehook set up');
app.use('/account', middlewareChain, routers.account);
logger.silly('account api set up');
app.use('/auxiliaryDocument', middlewareChain, routers.auxiliaryDocument);
logger.silly('auxdox api set up');
app.use('/problem', routers.problem);
logger.silly('problem router set up');
app.use('/navigationActivity', routers.navigationActivity);
logger.silly('activity router set up');
app.use('/quickView', middlewareChain, routers.quickView);
logger.silly('quickview api set up');
app.use('/health', routers.health);
app.use('/status', routers.health);
logger.silly('health api set up');
//}}}

//{{{ admin usage
app.use('/admin/alert', middlewareChain, routers.admin.alert);
logger.silly('alert administration api set up');
app.use('/admin/auxiliaryDocument', middlewareChain, routers.admin.auxiliaryDocument);
app.use('/admin/operatingProcedure', middlewareChain, routers.admin.operatingProcedure);
app.use('/admin/training', middlewareChain, routers.admin.training);
app.use('/admin/workflow', middlewareChain, routers.admin.workflow);
app.use('/admin/userManagement', middlewareChain, routers.admin.userManagement);
app.use('/admin/customer', middlewareChain, routers.admin.customer);
app.use('/admin/quiz', middlewareChain, routers.admin.quiz);
//}}}


modeler.setup(config.configuration.paths.dbListing(), function(err) {
    logger.info('modeling complete.');
    var server = http.createServer(app)
        .listen(app.get("port"), function() {
            logger.info("Express server listening on port " + app.get("port"));
            logger.info('ready for requests!');
        });
});
