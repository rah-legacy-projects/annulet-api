var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util;
require('annulet-util');
module.exports = exports = {
    list: function(req, res, next) {
        async.waterfall([

            function(cb) {
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                projectUtility.instance.operatingProcedure.status(models, {
                    customerUser: req.actionCustomerUser
                }, function(err, models, statuslist) {
                    p.operatingProcedures = statuslist;
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            res.data = p.operatingProcedures;
            res.err = err;
            next();
        });
    },
    get: function(req, res, next) {
        async.waterfall([

            function(cb) {
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                //is this a def or instance?
                async.parallel({
                    definition: function(cb) {
                        models.definitions.operatingProcedure.OperatingProcedure.findOne({
                            $or: [{
                                _id: req.params.operatingProcedure.toObjectId()
                            }, {
                                shortName: req.params.operatingProcedure
                            }]
                        })
                            .exec(function(err, op) {
                                cb(err, (op || {})
                                    ._id);
                            });
                    },
                    instance: function(cb) {
                        models.instances.operatingProcedure.OperatingProcedure.findOne({
                            _id: req.params.operatingProcedure.toObjectId()
                        })
                            .exec(function(err, op) {
                                cb(err, (op || {})
                                    .operatingProcedureDefinition);
                            });
                    }
                }, function(err, r) {
                    p.opToStamp = r.instance || r.definition;
                    if (!p.opToStamp) {
                        res.statusOverride = 404;
                        return cb('Could not find operating procedure for ' + req.params.operatingProcedure, models, p);
                    }
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //stamp the definition ID retrieved
                projectUtility.definition.operatingProcedure.stamper.stamper(models, {
                    operatingProcedureDefinition: p.opToStamp,
                    actionCustomerUser: req.actionCustomerUser,
                    customerUser: req.actionCustomerUser,
                    flatten: true
                }, function(err, models, stamped) {
                    p.operatingProcedure = stamped;
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            res.err = err;
            res.data = p.operatingProcedure;
            next();
        });
    },
    saveView: function(req, res, next) {
        logger.silly('saving ' + req.params.sectionType + ' (' + req.params.sectionRange + ')');
        async.waterfall([

            function(cb) {
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                models.instances.operatingProcedure.ranged.sectionTypes[req.params.sectionType].findOne({
                    _id: req.params.sectionRange.toObjectId()
                })
                    .exec(function(err, section) {
                        if (!!err) {
                            logger.error('problem getting ranged sectio for op: ' + util.inspect(err));
                        }
                        section.views++;
                        section.modifiedBy = req.actionCustomerUser._id.toString();
                        section.save(function(err, section) {
                            cb(err, section);
                        });
                    });
            }
        ], function(err, r) {
            if (!!err) {
                logger.error(req.params.sectionType + ' (' + req.params.section + ') problem saving section: ' + util.inspect(err));
            }
            res.err = err;
            res.data = r;
            next();
        });
    },
    finalize: function(req, res, next) {
        logger.silly('operatingProcedure range id: ' + req.params.operatingProcedureRange);
        async.waterfall([

            function(cb) {
                //get the models for the operatingProcedure
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                logger.silly('[operatingProcedure finalize] looking for operatingProcedure instance');
                models.instances.operatingProcedure.ranged.OperatingProcedure.findOne({
                    _id: req.params.operatingProcedureRange.toObjectId()
                })
                    .exec(function(err, operatingProcedureRange) {
                        p.operatingProcedureRange = operatingProcedureRange;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //mark the OP as complete
                p.operatingProcedureRange.completedDate = new Date();
                p.operatingProcedureRange.save(function(err, operatingProcedureRange) {
                    p.operatingProcedureRange = operatingProcedureRange;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //add an item activity for the finalization of the OP
                (new models.tracking.ItemActivity({
                    changedBy: req.actionCustomerUser._id.toString(),
                    state: 'Finalized',
                    itemType: 'instances.operatingProcedure.ranged.OperatingProcedure',
                    item: p.operatingProcedureRange._id,
                    message: {
                        firstName: req.actionCustomerUser.user.firstName,
                        lastName: req.actionCustomerUser.user.lastName,
                        email: req.actionCustomerUser.user.email,
                        itemTitle: p.operatingProcedureRange.title
                    }
                }))
                    .save(function(err) {
                        cb(err, models, p);
                    });
            }
        ], function(err, models, p) {
            if (!!err) {
                logger.error('[operatingProcedure finalize] error finalizing: ' + util.inspect(err));
            }

            res.err = err;
            res.data = {};
            next();
        });
    }
};
