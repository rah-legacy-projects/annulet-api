var logger = require('winston');

module.exports = {
    get401: function(req, res, next) {
        res.statusOverride = 401;
        res.data = 'test data';
        res.err = 'not authorized';
        next();
    },
    get403: function(req, res, next) {
        res.statusOverride = 403;
        res.data = 'verboten!';
        res.err = 'verboten!';
        next();
    },
    get402: function(req, res, next) {
        res.statusOverride = 402;
        res.err = '$$$ pay up';
        res.data = 'test data';
        next();
    },
    get404: function(req, res, next) {
        res.statusOverride = 404;
        res.err = 'not found';
        res.data = 'test data';
        next();
    },
    get409: function(req, res, next) {
        res.statusOverride = 409;
        res.err = 'conflict';
        res.data = 'test data';
        next();
    },
    get500: function(req, res, next) {
        res.statusOverride = 500;
        res.err = 'generic error';
        res.data = 'test data';
        next();
    },
    get501: function(req, res, next) {
        res.statusOverride = 501;
        res.err = 'not done yet';
        res.data = 'test data';
        next();
    },
    get502: function(req, res, next) {
        res.statusOverride = 502;
        res.err = 'bad gateway';
        res.data = 'test data';
        next();
    },
    get503: function(req, res, next) {
        res.statusOverride = 503;
        res.err = 'sorry, not home';
        res.data = 'test data';
        next();
    },
    get504: function(req, res, next) {
        res.statusOverride = 504;
        res.err = 'gateway timeout';
        res.data = 'test data';
        next();
    },
};
