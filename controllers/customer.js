var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    moment = require('moment');
require('annulet-config');
module.exports = exports = {
    list: function(req, res, next) {
        async.waterfall([

            function(cb) {
                projectUtility.auth.userCustomers({
                    user: req.loggedInUser._id
                }, function(err, ids) {
                    cb(err, ids);
                })
            },
            function(customerIds, cb) {
                async.parallel(_.map(customerIds, function(customerId) {
                    return function(cb) {
                        async.waterfall([

                            function(cb) {
                                modeler.db({
                                    collection: 'Customer',
                                    query: {
                                        _id: customerId.toObjectId(),
                                        active: true,
                                        deleted: false
                                    }
                                }, function(err, models) {
                                    if (!models) {
                                        cb({
                                            skip: true
                                        });
                                    } else {
                                        cb(err, models);
                                    }
                                });
                            },
                            function(models, cb) {
                                models.Customer.findOne({
                                    _id: customerId
                                })
                                    .exec(function(err, customer) {
                                        cb(err, models, {
                                            customer: customer
                                        });
                                    });
                            },
                            function(models, p, cb) {
                                models.auth.CustomerUser.findOne({
                                    customer: customerId,
                                    user: req.loggedInUser._id
                                })
                                    .exec(function(err, customerUser) {
                                        if (!customerUser) {
                                            cb({
                                                skip: true
                                            });
                                        } else {
                                            p.customerUser = customerUser;
                                            p.access = p.customer.getAccess(customerUser);
                                            cb(err, models, p);
                                        }
                                    });
                            }
                        ], function(err, models, p) {
                            if (!!err && !!err.skip) {
                                cb(null);
                            } else {
                                cb(err, p);
                            }
                        });
                    }
                }), function(err, r) {
                    var reduced = _.chain(r)
                        .filter(function(rr) {
                            return !!rr;
                        })
                        .map(function(rr) {
                            return {
                                access: rr.access,
                                id: rr.customer._id,
                                customerName: rr.customer.name
                            };
                        })
                        .value();
                    cb(err, reduced);
                });
            }
        ], function(err, p) {
            if(!!err){
                logger.error('[customer list] error getting customers: ' + util.inspect(err));
            }
            res.err = err;
            res.data = p;
            logger.silly('got customers');
            next();
        });
    },
    detail: function(req, res, next) {
        modeler.db({
            collection: 'Customer',
            query: {
                _id: req.params.customer.toObjectId()
            }
        }, function(err, models) {
            models.Customer.findOne({
                _id: req.params.customer
            })
                .exec(function(err, customer) {
                    res.err = err;
                    res.data = customer;
                    next();
                });
        });
    },

}
