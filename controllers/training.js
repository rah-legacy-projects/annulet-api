var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    util = require('util'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util;

_.mixin(require('annulet-util')
    .lodashMixins);

require('annulet-util');
module.exports = exports = {
    get: function(req, res, next) {
        logger.silly('getting models for training: ' + req.params.training);
        async.waterfall([

            function(cb) {
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                //is this a def or instance?
                async.parallel({
                    definition: function(cb) {
                        models.definitions.training.Training.findOne({
                            $or: [{
                                _id: req.params.training.toObjectId()
                            }, {
                                shortName: req.params.training
                            }]
                        })
                            .exec(function(err, root) {
                                cb(err, (root || {})
                                    ._id);
                            });
                    },
                    instance: function(cb) {
                        models.instances.training.Training.findOne({
                            _id: req.params.training.toObjectId()
                        })
                            .exec(function(err, root) {
                                cb(err, (root || {})
                                    .trainingDefinition);
                            });
                    }
                }, function(err, r) {
                    p.trainingToStamp = r.instance || r.definition;
                    p.trainingDefinition = r.definition;
                    if (!p.trainingToStamp) {
                        logger.error('no training to stamp found for ' + req.params.training);
                        res.statusOverride = 404;
                        return cb('Could not find training for ' + req.params.training, models, p);
                    }
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //stamp the definition ID retrieved
                projectUtility.definition.training.stamper.stamper(models, {
                    trainingDefinition: p.trainingToStamp,
                    actionCustomerUser: req.actionCustomerUser,
                    customerUser: req.actionCustomerUser,
                    flatten: true
                }, function(err, models, stamped) {
                    p.training = stamped;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //get the training wf def ranges
                models.definitions.workflow.ranged.workflowItemTypes.Training.find({
                    training: p.trainingDefinition
                })
                    .exec(function(err, trainingWorkflowDefRanges) {
                        logger.silly('[training get] def ranges: ' + trainingWorkflowDefRanges.length);
                        p.trainingWorkflowDefRanges = trainingWorkflowDefRanges;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //get the items those ranges correspond to
                models.definitions.workflow.workflowItemTypes.Training.find({
                    rangedData: {
                        $elemMatch: {
                            $in: _.pluck(p.trainingWorkflowDefRanges, '_id')
                        }
                    }
                })
                    .exec(function(err, trainingWorkflowDefs) {
                        logger.silly('[training get] defs: ' + trainingWorkflowDefs.length);
                        p.trainingWorkflowDefs = trainingWorkflowDefs;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //get the root of those defs
                async.parallel(_.map(p.trainingWorkflowDefs, function(trainingWorkflowDef) {
                    return function(cb) {
                        projectUtility.definition.workflow.rootForItem(models, {
                            workflowItem: trainingWorkflowDef
                        }, function(err, models, rootDef) {
                            cb(err, rootDef);
                        });
                    };
                }), function(err, r) {
                    logger.silly('[training get] got root defs: ' + r.length);
                    p.rootDefinitions = r;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //get the instances for the user for the def roots
                models.instances.workflow.workflowItemTypes.Root.find({
                    $and: [{
                        _id: {
                            $in: req.actionCustomerUser.workflowInstances
                        }
                    }, {
                        itemDefinition: {
                            $in: _.pluck(p.rootDefinitions, '_id')
                        }
                    }]
                })
                    .exec(function(err, rootInstances) {
                        logger.silly('[training get] got root instances: ' + rootInstances.length);
                        p.rootInstances = rootInstances;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //load those wf instances
                async.parallel(_.map(p.rootInstances, function(rootInstance) {
                    return function(cb) {
                        projectUtility.instance.workflow.loader(models, {
                            workflowItemInstance: rootInstance,
                            flatten: true
                        }, function(err, models, wfInstance) {
                            cb(err, wfInstance);
                        });
                    };
                }), function(err, r) {
                    logger.silly('[training get] got wf instances from loader: ' + r.length);
                    p.workflowInstances = r;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //find the training item instances for the training item defs
                var instanceRanges = _.chain(p.workflowInstances)
                    .map(function(workflowInstance) {
                        return _.filterDeep(workflowInstance, {
                            itemDefinitionRange: function(key, value) {
                                return _.any(p.trainingWorkflowDefRanges, function(defRange) {
                                    return defRange._id.toString() == (value || '')
                                        .toString();
                                });
                            }
                        });
                    })
                    .reduce(function(a, b) {
                        return a.concat(b);
                    }, [])
                    .compact()
                    .value();

                logger.silly('[training get] got instance ranges, about to save: ' + instanceRanges.length);

                var instanceRangeIds = _.map(instanceRanges, function(instanceRange) {
                    return instanceRange._rangeId;//.toObjectId();
                });

                //load the instances ranges
                models.instances.workflow.ranged.workflowItemTypes.Training.find({
                    _id: {
                        $in: instanceRangeIds
                    }
                })
                    .exec(function(err, trainingInstanceRanges) {
                        logger.silly('[training get] training instance ranges: ' + trainingInstanceRanges.length);
                        p.trainingInstanceRanges = trainingInstanceRanges;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //save the stamped training in those instances
                async.parallel(_.map(p.trainingInstanceRanges, function(range) {
                    return function(cb) {
                        range.training = p.training._id;
                        range.save(function(err, range) {
                            cb(err, range);
                        });
                    };
                }), function(err, r) {
                    logger.silly('[training get] ranges saved: ' + r.length);
                    logger.silly('[training get] training id: ' + p.training._id);
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            if (err) {
                logger.error('[training get] ' + util.inspect(err));
            }
            res.err = err;
            if (!!p) {
                res.data = p.training;
            }
            next();
        });
    },
    saveView: function(req, res, next) {
        logger.silly('saving ' + req.params.sectionType + ' (' + req.params.sectionRange + ')');
        async.waterfall([

            function(cb) {
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                models.auth.CustomerUser.findOne({
                    user: req.loggedInUser._id.toObjectId(),
                })
                    .exec(function(err, customerUser) {
                        cb(err, models, {
                            customerUser: customerUser
                        });
                    });
            },
            function(models, p, cb) {
                models.instances.training.ranged.sectionTypes[req.params.sectionType].findOne({
                    _id: req.params.sectionRange.toObjectId()
                })
                    .exec(function(err, section) {
                        logger.silly('pre-update, section ' + section._id + ' views: ' + section.views);
                        if (!!err) {
                            logger.error(util.inspect(err));
                        }
                        section.modifiedBy = p.customerUser._id.toString();
                        section.views++;
                        section.save(function(err, section) {
                            if (!!err) {
                                logger.error('problem saving viewupdate: ' + util.inspect(err));
                            }
                            cb(err, section);
                        });
                    });
            }
        ], function(err, r) {
            if (!!err) {
                logger.error(req.params.sectionType + ' (' + req.params.sectionRange + ') problem saving section: ' + util.inspect(err));
            }
            res.err = err;
            res.data = null;
            next();
        });
    },
    finalize: function(req, res, next) {
        logger.silly('training id: ' + req.params.trainingRange);
        async.waterfall([

            function(cb) {
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                //find the training range
                logger.silly('[training finalize] getting training range');
                models.instances.training.ranged.Training.findOne({
                    _id: req.params.trainingRange.toObjectId()
                })
                    .exec(function(err, trainingRange) {
                        p.trainingRange = trainingRange;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //find the training instance
                models.instances.training.Training.findOne({
                    rangedData: p.trainingRange._id
                })
                    .exec(function(err, training) {
                        logger.silly('[training finalize] got training: ' + !!training);
                        p.training = training;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //find the training wf item instance range
                models.instances.workflow.ranged.workflowItemTypes.Training.findOne({
                    training: p.training._id
                })
                    .exec(function(err, workflowItemRange) {
                        logger.silly('[training finalize] wf item range found: '+ !!workflowItemRange);
                        p.workflowItemRange = workflowItemRange;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb){
                //find the training wf item instance range's item
                models.instances.workflow.workflowItemTypes.Training.findOne({
                    rangedData:p.workflowItemRange._id
                }).exec(function(err, workflowItem){
                    p.workflowItem = workflowItem;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //complete the training
                p.trainingRange.completedDate = moment()
                    .toDate();
                p.trainingRange.modifiedBy = req.actionCustomerUser._id.toString();
                p.trainingRange.save(function(err, trainingRange) {
                    p.trainingRange = trainingRange;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //complete the wf item
                p.workflowItemRange.completedDate = moment()
                    .toDate();
                p.workflowItemRange.modifiedBy = req.actionCustomerUser._id.toString();
                p.workflowItemRange.save(function(err, workflowItemRange) {
                    p.workflowItemRange = workflowItemRange;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //get the wf root item
                projectUtility.instance.workflow.rootForItem(models, {
                    workflowItem: p.workflowItem
                }, function(err, models, rootItem) {
                    logger.silly('[training finalize] root: ' + util.inspect(rootItem));
                    p.rootItem = rootItem;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //add an item activity for the finalization of the training
                (new models.tracking.ItemActivity({
                    changedBy: req.actionCustomerUser._id.toString(),
                    state: 'Finalized',
                    itemType: 'instances.training.ranged.Training',
                    item: p.trainingRange._id,
                    message: {
                        firstName: req.actionCustomerUser.user.firstName,
                        lastName: req.actionCustomerUser.user.lastName,
                        email: req.actionCustomerUser.user.email,
                        itemTitle: p.training.title
                    }
                }))
                    .save(function(err) {
                        cb(err, models, p);
                    });
            }
        ], function(err, models, p) {
            if (!!err) {
                logger.error('[training finalize] error finalizing: ' + util.inspect(err));
            }
            logger.silly('wf item: ' + p.rootItem._id);
            res.err = err;
            res.data = {
                workflowItem: p.rootItem._id
            };
            next();
        });
    }
};
