var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    moment = require('moment'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    config = require('annulet-config'),
    request = require('request');

require('annulet-util');

var getComplaintWithMakers = function(models, item, cb) {
    logger.silly('loading ' + item._id);
    var baseUrl = config.configuration.paths.authUri() + '/user/get/';
    async.waterfall([

        function(cb) {
            projectUtility.complaint.loadComplaint(models, {
                complaint: item._id.toObjectId()
            }, function(err, models, complaint) {
                if (!complaint) {
                    return cb(err);
                }
                cb(err, models, {
                    complaint: complaint.toObject()
                });
            });
        },
        function(models, p, cb) {
            //get created by for the complaint
            logger.silly('[mc] complaint created by: ' + p.complaint.createdByCustomerUser.user);
            request.get({
                url: baseUrl + p.complaint.createdByCustomerUser.user,
                headers: {
                    'annulet-internal': config.configuration.paths.internalKey()
                },
                json: true
            }, function(err, response, body) {
                p.complaint.createdByCustomerUser = body.data;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[mc] about to get madeby for changes');
            //get created for changes
            async.parallel(_.map(p.complaint.changes, function(change) {
                logger.silly('[mc] change madeBy: ' + change.madeBy.user);
                return function(cb) {
                    var baseUrl = config.configuration.paths.authUri() + '/user/get/';
                    //get created by for the complaint
                    request.get({
                        url: baseUrl + change.madeBy.user,
                        headers: {
                            'annulet-internal': config.configuration.paths.internalKey()
                        },
                        json: true
                    }, function(err, response, body) {
                        change.madeBy = body.data;
                        cb(err);
                    });
                };
            }), function(err, r) {
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //get created for comments
            async.parallel(_.map(p.complaint.comments, function(comment) {
                logger.silly('[mc] comment madeBy: ' + comment.madeBy.user);
                return function(cb) {
                    //get created by for the complaint
                    request.get({
                        url: baseUrl + comment.madeBy.user,
                        headers: {
                            'annulet-internal': config.configuration.paths.internalKey()
                        },
                        json: true
                    }, function(err, response, body) {
                        comment.madeBy = body.data;
                        cb(err);
                    });
                };
            }), function(err, r) {
                cb(err, models, p);
            });
        },
    ], function(err, models, p) {
        cb(err, models, (p || {})
            .complaint);
    });
};

module.exports = exports = {
    list: function(req, res, next) {
        logger.silly('getting complaint listing for ' + req.customer);

        async.waterfall([

            function(cb) {
                cb(null, req.models);
            },
            function(models, cb) {


                //query parameters:
                //{
                //  echo: request #
                //  search: what to search for
                //  filters: key-value pairs of what to search for
                //  limit: #of records to return
                //  skip: #of records to skip before returning
                //  orderByClauses: key-value pairs; keys for sorting, the value for direction
                //}


                var defaults = {
                    echo: 0,
                    filters: [],
                    limit: 10,
                    skip: 0,
                    orderByClauses: [{
                        name: 'created',
                        direction: 'ascending'
                    }]
                }

                var options = req.body;
                _.defaults(options, defaults);

                //hack: qs converts to string, convert back to int for slicing parameters
                options.limit = parseInt(options.limit, 10);
                options.skip = parseInt(options.skip, 10);

                //edge case: limit + skip < 0
                if (options.limit < 0) {
                    options.limit = 10;
                }
                if (options.skip < 0) {
                    options.skip = 0;
                }

                //build the search query or advanced search query
                var query = {
                    customer: req.customer
                };
                if (!!options.advancedSearch) {
                    query.$or = [];
                    _.each(options.advancedSearch, function(as) {
                        if ((as.search || '') != '') {
                            //query[key] = new RegExp(options.search, 'i');
                            var iq = {};
                            iq[as.on] = {
                                $regex: new RegExp(options.search, 'i')
                            }
                            query.$or.push(iq);
                        }

                    })
                } else {
                    query.$or = [];
                    _.chain(models.complaints.Complaint.schema.paths)
                        .keys()
                        .difference([
                            'changes',
                            'comments',
                            'id',
                            '_id',
                            '__v',
                            'customer',
                            'createdByCustomerUser',
                            'created',
                            'dateReceived',
                            'dateResponseSent',
                            'modified'
                        ])
                        .each(function(key) {
                            if ((options.search || '') != '') {
                                //query[key] = new RegExp(options.search, 'i');
                                var iq = {};
                                iq[key] = {
                                    $regex: new RegExp(options.search, 'i')
                                }
                                query.$or.push(iq);
                            }
                        })
                        .value();
                }

                if (!!query.$or && query.$or.length == 0) {
                    delete query.$or;
                }

                async.parallel({
                    total: function(cb) {
                        models.complaints.Complaint.count({
                            customer: req.customer
                        }, cb);
                    },
                    queryTotal: function(cb) {
                        models.complaints.Complaint.count(query, cb)
                    },
                    items: function(cb) {
                        //make the sort string
                        var sortByString = _.reduce(options.orderByClauses, function(memo, clause) {
                            if (/asc|ascending/i.test(clause.direction)) {
                                memo += ' ' + clause.name;
                            } else if (/desc|descending/i.test(clause.direction)) {
                                memo += ' -' + clause.name;
                            }
                        }, '');
                        models.complaints.Complaint.find(query)
                            .sort(sortByString)
                            .skip(options.skip)
                            .limit(options.limit)
                            .exec(function(err, items) {

                                //hack: use the complaint loader to load the entirety of the complaints
                                async.parallel(_.map(items, function(item) {
                                    return function(cb) {
                                        getComplaintWithMakers(models, {
                                            _id: item._id.toString()
                                        }, function(err, models, complaint) {
                                            cb(err, complaint);
                                        });
                                    };
                                }), function(err, complaints) {
                                    items = complaints || [];
                                    cb(err, items);
                                });
                            });
                    }
                }, function(err, r) {
                    //response expected:
                    //{
                    //  errors: errors, if any
                    //  filtered records: #of filtered records before slicing
                    //  total records: total number of records without slicing
                    //  echo: respond with the same thing as was sent.  Prevents binding issues.
                    //  record set: the records to return
                    //}
                    var response = {
                        errors: !!err ? [err] : [],
                        filteredRecords: r.queryTotal,
                        totalRecords: r.total,
                        echo: options.echo,
                        items: r.items
                    };
                    cb(err, response);
                });



            }
        ], function(err, p) {
            res.err = err;
            res.data = p;
            next();
        });

    },
    recent: function(req, res, next) {
        logger.silly('getting recent complaint listing for ' + req.customer);

        async.waterfall([

            function(cb) {
                cb(null, req.models);
            },
            function(models, cb) {
                models.complaints.Complaint.find({
                    customer: req.customer,
                    modified: {
                        $gte: moment()
                            .subtract(7, 'days')
                    }

                })
                    .sort('-modified')
                    .exec(function(err, items) {
                        if (!!err) {
                            logger.error('problem finding recent complaints: ' + util.inspect(err));
                        }

                        var howManyMore = items.length - 5;
                        if (howManyMore < 0) {
                            howManyMore = 0;
                        }
                        //hack: use the complaint loader to load the entirety of the complaints
                        async.parallel(_.map(items.slice(0, 5), function(item) {
                            return function(cb) {
                                getComplaintWithMakers(models, {
                                    _id: item._id.toString()
                                }, function(err, models, complaint) {
                                    cb(err, complaint);
                                });
                            };
                        }), function(err, complaints) {
                            items = complaints || [];
                            cb(err, {
                                howManyMore: howManyMore,
                                complaints: complaints
                            });
                        });
                    });
            }
        ], function(err, p) {
            logger.silly('recent complaints retrieved.');
            res.data = p;
            res.err = err;
            next();
        });

    },
    detail: function(req, res, next) {
        if (!/^[0-9a-fA-F]{24}$/.test(req.params.complaint)) {
            res.statusOverride = 404;
            res.err = 'Invalid complaint ID.';
            next();
        } else {
            logger.silly('complaint detail');
            getComplaintWithMakers(null, {
                _id: req.params.complaint
            }, function(err, models, complaint) {
                res.err = err;
                if (!complaint) {
                    res.err = 'Complaint not found.';
                    res.statusOverride = 404;
                }
                res.data = complaint;
                next();
            });
        }
    },
    addComment: function(req, res, next) {
        projectUtility.complaint.loadComplaint(req.models, {
            complaint: req.params.complaint.toObjectId()
        }, function(err, models, complaint) {
            models.auth.CustomerUser.findOne({
                _id: req.actionCustomerUser._id
            })
                .exec(function(err, customerUser) {
                    var newComment = new models.complaints.Comment(req.body);
                    newComment.createdBy = customerUser._id.toString();
                    newComment.modifiedBy = customerUser._id.toString();
                    newComment.madeBy = customerUser._id;
                    newComment.save(function(err, savedComment) {
                        var commentObject = savedComment.toObject();
                        commentObject.madeBy = req.actionCustomerUser.user;
                        complaint.comments.push(savedComment);
                        complaint.modifiedBy = customerUser._id.toString();
                        complaint.save(function(err, complaint) {
                            res.err = err;
                            res.data = commentObject;
                            next();
                        });
                    });
                });
        });

    },
    updateComplaint: function(req, res, next) {
        logger.silly('about to update complaint');
        async.waterfall([

            function(cb) {
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                projectUtility.complaint.manageComplaint(null, {
                    customer: req.customer.toObjectId(),
                    customerUser: req.actionCustomerUser,
                    complaint: req.body,
                }, function(err, models, complaint) {
                    if (!!err) {
                        logger.error('problem saving complaint: ' + util.inspect(err));
                    }
                    p.complaint = complaint;
                    cb(err, models, p);
                });

            },
            function(models, p, cb) {
                getComplaintWithMakers(models, {
                    _id: p.complaint._id.toString()
                }, function(err, models, complaint) {
                    p.complaint = complaint;
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            res.err = err;
            res.data = p.complaint;
            next();
        });
    }
}
