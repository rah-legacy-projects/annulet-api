var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    moment = require('moment');
require('annulet-config');
module.exports = exports = {
    list: function(req, res, next) {
        async.waterfall([

            function(cb) {
                projectUtility.customer.list(null, function(err, customers) {
                    cb(err, customers);
                });
            }
        ], function(err, p) {
            res.err = err;
            res.data = p;
            next();
        });
    },
    detail: function(req, res, next) {
        modeler.db({
            collection: 'Customer',
            query: {
                _id: req.params.customer.toObjectId()
            }
        }, function(err, models) {
            models.Customer.findOne({
                _id: req.params.customer
            })
                .exec(function(err, customer) {
                    res.err = err;
                    res.data = customer;
                    next();
                });
        });
    },

}
