var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    request = require('request'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    config = require('annulet-config'),
    Mandrill = require('mandrill-api')
    .Mandrill;
require('annulet-util');
module.exports = exports = {
    list: function(req, res, next) {
        async.waterfall([

            function(cb) {
                cb(null, req.models, {})
            },
            function(models, p, cb) {
                //get the active/undeleted quiz
                models.definitions.quiz.Quiz.find({
                    active: true,
                    deleted: false
                }, function(err, quiz) {
                    p.quiz = quiz;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //separate drafts from published
                p.drafts = _.filter(p.quiz, function(quiz) {
                    return !!quiz.isDraftOf;
                });
                p.published = _.filter(p.quiz, function(quiz) {
                    return !quiz.isDraftOf;
                });
                cb(null, models, p);
            },
            function(models, p, cb) {
                if(p.published.length == 0){
                    return cb(null, models, p);
                }
                //load the published flattened
                projectUtility.definition.quiz.loader(models, {
                    actionCustomerUser: req.actionCustomerUser,
                    quizDefinitions: p.published,
                    flattened: true,
                }, function(err, models, quiz) {
                    p.published = quiz;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                if(p.drafts.length == 0){
                    return cb(null, models, p);
                }
                //load the drafts unflattened
                projectUtility.definition.quiz.loader(models, {
                    actionCustomerUser: req.actionCustomerUser,
                    quizDefinitions: p.drafts,
                    flattened: false,
                }, function(err, models, quiz) {
                    p.drafts = quiz;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //make {published, draft} tuples

                var tuples = [];
                //take care of the new and draft tuples
                tuples = tuples.concat(_.map(p.drafts, function(draft) {
                    return {
                        draft: draft,
                        published: _.find(p.published, function(published) {
                            return published._id.toString() == draft.isDraftOf.toString();
                        })
                    };
                }));
                //filter the published to the undrafted published
                //map to tuples
                tuples = tuples.concat(_.chain(p.published)
                    .filter(function(published) {
                        return !_.any(p.drafts, function(draft) {
                            return published._id.toString() == draft.isDraftOf.toString();
                        });
                    })
                    .map(function(published) {
                        return {
                            draft: null,
                            published: published
                        };
                    })
                    .value());

                p.tuples = tuples;
                cb(null, models, p);
            }
        ], function(err, models, p) {
            res.err = err;
            res.data = p.tuples;
            next();
        });
    },
    load: function(req, res, next) {
        projectUtility.definition.quiz.loader(req.models, {
            actionCustomerUser: req.customerUser,
            quizDefinition: req.params.quiz
        }, function(err, models, quiz) {
            res.err = err;
            res.data = quiz;
            next();
        });
    },
    loadByShortName: function(req, res, next) {
        async.waterfall([

            function(cb) {
                projectUtility.definition.quiz.loadByShortName(req.models, {
                    customer: req.customer,
                    shortName: req.params.shortName
                }, function(err, models, quizzes) {
                    cb(err, models, {
                        quizzes: quizzes
                    });
                });
            },
            function(models, p, cb) {
                //there should be zero, one or two quizzes
                var published = _.filter(p.quizzes, function(quiz) {
                        return !quiz.isDraftOf;
                    }),
                    drafts = _.filter(p.quizzes, function(quiz) {
                        return !!quiz.isDraftOf;
                    });

                if (published.length > 1) {
                    //more than one published quiz has the same shortname.
                    res.statusOverride = 400;
                    return cb('Customer ' + util.inspect(req.customer) + ' has more than one published quiz for ' + req.params.shortName);
                }
                if (drafts.length > 1) {
                    //more than one published quiz has the same shortname.
                    res.statusOverride = 400;
                    return cb('Customer ' + util.inspect(req.customer) + ' has more than one drafted quiz for ' + req.params.shortName);
                }

                p.tuple = {
                    published: _.first(published),
                    draft: _.first(drafts)
                };
                cb(null, models, p);
            },
        ], function(err, models, p) {
            res.err = err;
            res.data = p.tuple;
            next();
        });

    },
    create: function(req, res, next) {
        projectUtility.definition.quiz.draft.create(req.models, {
            customer: req.customer,
            quizDefinition: req.body,
            actionCustomerUser: req.actionCustomerUser
        }, function(err, models, draft) {
            res.err = err;
            res.data = draft;
            next();
        });
    },
    update: function(req, res, next) {
        projectUtility.definition.quiz.draft.update(req.models, {
            customer: req.customer,
            quizDefinition: req.body, //todo: use req.body for quiz def in admin?
            actionCustomerUser: req.actionCustomerUser,
            flatten: true
        }, function(err, models, draft) {
            res.err = err;
            res.data = draft;
            next();
        });
    },
    publish: function(req, res, next) {
        projectUtility.definition.quiz.draft.publish(req.models, {
            quizDefinition: req.body,
            customer: req.customer,
            actionCustomerUser: req.actionCustomerUser
        }, function(err, models, published) {
            res.err = err;
            res.data = published;
            next();
        });
    },
    close: function(req, res, next) {
        projectUtility.definition.quiz.draft.close(req.models, {
            quizDefinition: req.body,
            customer: req.customer,
            actionCustomerUser: req.actionCustomerUser
        }, function(err, models, closedDraft) {
            res.err = err;
            res.data = closedDraft._id;
            next();
        });
    },
}
