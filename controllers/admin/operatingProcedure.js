var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    request = require('request'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    config = require('annulet-config'),
    Mandrill = require('mandrill-api')
    .Mandrill;
require('annulet-util');
module.exports = exports = {
    list: function(req, res, next) {
        async.waterfall([

            function(cb) {
                cb(null, req.models, {})
            },
            function(models, p, cb) {
                logger.silly('include all ops in listing: ' + req.query.all);
                var query = {
                    deleted: false
                };
                if (!!req.query.all && req.query.all == 'true') {
                    //leave the query alone
                } else {
                    query.active = true;
                }

                //get the active/undeleted operatingProcedures
                models.definitions.operatingProcedure.OperatingProcedure.find(query, function(err, operatingProcedures) {
                    p.operatingProcedures = operatingProcedures;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //separate drafts from published
                p.drafts = _.filter(p.operatingProcedures, function(operatingProcedure) {
                    return !!operatingProcedure.isDraftOf;
                });
                p.published = _.filter(p.operatingProcedures, function(operatingProcedure) {
                    return !operatingProcedure.isDraftOf;
                });

                cb(null, models, p);
            },
            function(models, p, cb) {
                if (p.published.length == 0) {
                    return cb(null, models, p);
                }
                //load the published flattened
                projectUtility.definition.operatingProcedure.loader(models, {
                    actionCustomerUser: req.actionCustomerUser,
                    operatingProcedureDefinitions: p.published,
                    flattened: true,
                }, function(err, models, operatingProcedures) {
                    p.published = operatingProcedures;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                if (p.drafts.length == 0) {
                    return cb(null, models, p);
                }
                //load the drafts unflattened
                projectUtility.definition.operatingProcedure.loader(models, {
                    actionCustomerUser: req.actionCustomerUser,
                    operatingProcedureDefinitions: p.drafts,
                    flattened: false,
                }, function(err, models, operatingProcedures) {
                    p.drafts = operatingProcedures;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //make {published, draft} tuples

                var tuples = [];
                //take care of the new and draft tuples
                tuples = tuples.concat(_.map(p.drafts, function(draft) {
                    return {
                        draft: draft,
                        published: _.find(p.published, function(published) {
                            return published._id.toString() == draft.isDraftOf.toString();
                        })
                    };
                }));
                //filter the published to the undrafted published
                //map to tuples
                tuples = tuples.concat(_.chain(p.published)
                    .filter(function(published) {
                        return !_.any(p.drafts, function(draft) {
                            return published._id.toString() == draft.isDraftOf.toString();
                        });
                    })
                    .map(function(published) {
                        return {
                            draft: null,
                            published: published
                        };
                    })
                    .value());

                p.tuples = tuples;
                cb(null, models, p);
            }
        ], function(err, models, p) {
            res.err = err;
            res.data = p.tuples;
            next();
        });
    },
    load: function(req, res, next) {
        projectUtility.definition.operatingProcedure.loader(req.models, {
            actionCustomerUser: req.customerUser,
            operatingProcedureDefinition: req.params.operatingProcedure
        }, function(err, models, operatingProcedure) {
            res.err = err;
            res.data = operatingProcedure;
            next();
        });
    },
    loadByShortName: function(req, res, next) {
        async.waterfall([

            function(cb) {
                projectUtility.definition.operatingProcedure.loadByShortName(req.models, {
                    customer: req.customer,
                    shortName: req.params.shortName
                }, function(err, models, operatingProcedures) {
                    cb(err, models, {
                        operatingProcedures: operatingProcedures
                    });
                });
            },
            function(models, p, cb) {
                //there should be zero, one or two operatingProcedures
                var published = _.filter(p.operatingProcedures, function(operatingProcedure) {
                        return !operatingProcedure.isDraftOf;
                    }),
                    drafts = _.filter(p.operatingProcedures, function(operatingProcedure) {
                        return !!operatingProcedure.isDraftOf;
                    });

                if (published.length > 1) {
                    //more than one published operatingProcedure has the same shortname.
                    res.statusOverride = 400;
                    return cb('Customer ' + util.inspect(req.customer) + ' has more than one published operatingProcedure for ' + req.params.shortName);
                }
                if (drafts.length > 1) {
                    //more than one published operatingProcedure has the same shortname.
                    res.statusOverride = 400;
                    return cb('Customer ' + util.inspect(req.customer) + ' has more than one drafted operatingProcedure for ' + req.params.shortName);
                }

                p.tuple = {
                    published: _.first(published),
                    draft: _.first(drafts)
                };
                cb(null, p);
            },
        ], function(err, p) {
            res.err = err;
            res.data = p.tuple;
            next();
        });

    },

    create: function(req, res, next) {
        projectUtility.definition.operatingProcedure.draft.create(req.models, {
            customer: req.customer,
            operatingProcedureDefinition: req.body,
            actionCustomerUser: req.actionCustomerUser
        }, function(err, models, draft) {
            res.err = err;
            res.data = draft;
            next();
        });
    },
    update: function(req, res, next) {
        projectUtility.definition.operatingProcedure.draft.update(req.models, {
            customer: req.customer,
            operatingProcedureDefinition: req.body, //todo: use req.body for operatingProcedure def in admin?
            actionCustomerUser: req.actionCustomerUser,
            flatten: true
        }, function(err, models, draft) {
            res.err = err;
            res.data = draft;
            next();
        });
    },
    publish: function(req, res, next) {
        logger.silly('publishing OP draft');
        projectUtility.definition.operatingProcedure.draft.publish(req.models, {
            operatingProcedureDefinition: req.body,
            customer: req.customer,
            actionCustomerUser: req.actionCustomerUser
        }, function(err, models, published) {
            res.err = err;
            res.data = published;
            next();
        });
    },
    close: function(req, res, next) {
        projectUtility.definition.operatingProcedure.draft.close(req.models, {
            operatingProcedureDefinition: req.body,
            customer: req.customer,
            actionCustomerUser: req.actionCustomerUser
        }, function(err, models, closedDraft) {
            res.err = err;
            res.data = closedDraft._id;
            next();
        });
    },
    activate: function(req, res, next) {
        async.waterfall([

            function(cb) {
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                //find the OP to activate
                projectUtility.definition.operatingProcedure.loader(models, {
                    operatingProcedureDefinition: req.params.operatingProcedure,
                    customerUser: req.customerUser,
                    flatten: false
                }, function(err, models, op) {
                    p.operatingProcedureDefinition = op;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //identify the OP instances that belong to the loaded def
                models.instances.operatingProcedure.OperatingProcedure.find({
                    deleted: false,
                    operatingProcedureDefinition: p.operatingProcedureDefinition._id
                })
                    .exec(function(err, opInstances) {
                        p.operatingProcedureInstances = opInstances;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //activate the OP!
                p.operatingProcedureDefinition.active = true;
                p.operatingProcedureDefinition.modifiedBy = req.actionCustomerUser._id;
                cb(null, models, p);
            },
            function(models, p, cb) {
                //activate the OP instances
                _.each(p.operatingProcedureInstances, function(opInstance) {
                    opInstance.modifiedBy = req.actionCustomerUser._id;
                    opInstance.active = true;
                });
                cb(null, models, p);
            },
            function(models, p, cb) {
                //save the OP def
                p.operatingProcedureDefinition.save(function(err, op) {
                    p.operatingProcedureDefinition = op;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //save the OP instances
                async.parallel(_.map(p.operatingProcedureInstances, function(opInstance) {
                    return function(cb) {
                        opInstance.save(cb);
                    };
                }), function(err, r) {
                    p.operatingProcedureInstances = r;
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            res.err = err;
            res.data = p.operatingProcedureDefinition;
            next();
        });
    },
    deactivate: function(req, res, next) {
        async.waterfall([

            function(cb) {
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                //find the OP to activate
                projectUtility.definition.operatingProcedure.loader(models, {
                    operatingProcedureDefinition: req.params.operatingProcedure,
                    flatten: false
                }, function(err, models, op) {
                    p.operatingProcedureDefinition = op;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //identify the OP instances that belong to the loaded def
                models.instances.operatingProcedure.OperatingProcedure.find({
                    deleted: false,
                    operatingProcedureDefinition: p.operatingProcedureDefinition._id
                })
                    .exec(function(err, opInstances) {
                        p.operatingProcedureInstances = opInstances;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //activate the OP!
                p.operatingProcedureDefinition.active = false;
                p.operatingProcedureDefinition.modifiedBy = req.actionCustomerUser._id;
                cb(null, models, p);
            },
            function(models, p, cb) {
                //activate the OP instances
                _.each(p.operatingProcedureInstances, function(opInstance) {
                    opInstance.active = false;
                    opInstance.modifiedBy = req.actionCustomerUser._id;
                });
                cb(null, models, p);
            },
            function(models, p, cb) {
                //save the OP def
                p.operatingProcedureDefinition.save(function(err, op) {
                    logger.silly('deactivated op : ' + util.inspect(op));
                    p.operatingProcedureDefinition = op;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                projectUtility.definition.operatingProcedure.loader(models, {
                    operatingProcedureDefinition: req.params.operatingProcedure,
                    flatten: false
                }, function(err, models, op) {
                    logger.silly('experiment tiiiime: ' + util.inspect(op));
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //save the OP instances
                async.parallel(_.map(p.operatingProcedureInstances, function(opInstance) {
                    return function(cb) {
                        opInstance.save(cb);
                    };
                }), function(err, r) {
                    p.operatingProcedureInstances = r;
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            res.err = err;
            res.data = p.operatingProcedureDefinition;
            next();
        });
    }
}
