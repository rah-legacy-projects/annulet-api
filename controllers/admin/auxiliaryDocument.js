var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    path = require('path'),
    Busboy = require('busboy'),
    mime = require('mime'),
    request = require('request'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    config = require('annulet-config'),
    Mandrill = require('mandrill-api')
    .Mandrill;
require('annulet-util');
module.exports = exports = {
    list: function(req, res, next) {
        projectUtility.definition.auxiliaryDocument.list(req.models, {
            actionCustomerUser: req.actionCustomerUser
        }, function(err, models, tuples) {
            logger.silly('tuples loaded.');
            res.err = err;
            res.data = tuples;
            next();
        });
    },
    load: function(req, res, next) {
        projectUtility.definition.auxiliaryDocument.loader(req.models, {
            actionCustomerUser: req.customerUser,
            auxiliaryDocumentDefinition: req.params.auxiliaryDocument
        }, function(err, models, auxiliaryDocument) {
            res.err = err;
            res.data = auxiliaryDocument;
            next();
        });
    },
    loadTuple: function(req, res, next) {
        logger.silly('loading tuple: ' + req.params.auxiliaryDocument);
        logger.silly('models: ' + !!req.models);
        projectUtility.definition.auxiliaryDocument.tupleLoader(req.models, {
            actionCustomerUser: req.actionCustomerUser,
            auxiliaryDocument: req.params.auxiliaryDocument
        }, function(err, models, tuple) {
            res.err = err;
            res.data = tuple;
            logger.silly('tuple loaded.');
            next();
        });
    },
    create: function(req, res, next) {
        projectUtility.definition.auxiliaryDocument.draft.create(req.models, {
            customer: req.customer,
            auxiliaryDocumentDefinition: req.body,
            actionCustomerUser: req.actionCustomerUser
        }, function(err, models, draft) {
            res.err = err;
            res.data = draft;
            next();
        });
    },
    update: function(req, res, next) {
        projectUtility.definition.auxiliaryDocument.draft.update(req.models, {
            customer: req.customer,
            auxiliaryDocumentDefinition: req.body, //todo: use req.body for auxiliaryDocument def in admin?
            actionCustomerUser: req.actionCustomerUser,
            flatten: true
        }, function(err, models, draft) {
            res.err = err;
            res.data = draft;
            next();
        });
    },
    upload: function(req, res) {
        logger.silly('[auxdox upload] starting!');
        var _returns = 0;
        var _files = 0;
        var outercb = function(err, doc) {
            _returns++;
            logger.silly('[auxdox] outer callback called');

            if (_returns === _files + 1) {
                if (!!err) {
                    logger.silly('error: ' + util.inspect(err));
                    res.json(500, {
                        detail: error
                    });
                } else if (_files === 0) {
                    logger.silly('no files attached');
                    res.json(400, {
                        detail: 'No file attached.'
                    });
                } else {
                    logger.silly('ta-done!');
                    res.status(200)
                        .json({
                            data: doc
                        });
                }
            }
        };

        var busboy = new Busboy({
            headers: req.headers,
        });

        busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
            logger.silly('[auxdox upload] file event triggered');
            if (fieldname === 'file') {
                _files++;
                logger.silly('[auxdox upload] writing stream...');
                var ws = req.models.grid.createWriteStream({
                    mode: 'w',
                    content_type: mimetype,
                    filename: filename,
                    root: 'C' + req.customer
                });
                ws.on('close', function(file) {
                    outercb(null, {
                        _id: file._id,
                        displayName: filename,
                        extension: mime.extension(mimetype)
                    });
                });
                logger.silly('[auxdox upload] piping...');
                file.pipe(ws);
            } else {
                logger.silly('[auxdox upload] fieldname was ' + fieldname + ', skipping');
                file.resume();
            }
        });
        busboy.on('finish', function() {
            logger.silly('[auxdox upload] finish triggered');
            outercb();
        });

        req.pipe(busboy);
    },
    publish: function(req, res, next) {
        logger.silly('auxdox publish body: ' + util.inspect(req.body));
        projectUtility.definition.auxiliaryDocument.draft.publish(req.models, {
            auxiliaryDocumentDefinition: req.body,
            customer: req.customer,
            actionCustomerUser: req.actionCustomerUser
        }, function(err, models, published) {
            res.err = err;
            res.data = published;
            next();
        });
    },
    close: function(req, res, next) {
        projectUtility.definition.auxiliaryDocument.draft.close(req.models, {
            auxiliaryDocumentDefinition: req.body,
            customer: req.customer,
            actionCustomerUser: req.actionCustomerUser
        }, function(err, models, closedDraft) {
            res.err = err;
            res.data = closedDraft._id;
            next();
        });
    },
}
