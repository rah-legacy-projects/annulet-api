var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    request = require('request'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    config = require('annulet-config'),
    Mandrill = require('mandrill-api')
    .Mandrill;
require('annulet-util');
module.exports = exports = {
    list: function(req, res, next) {
        logger.silly('[wf admin list] abotu to check for wfs');
        async.waterfall([

            function(cb) {
                cb(null, req.models, {})
            },
            function(models, p, cb) {
                //get the active/undeleted workflows
                models.definitions.workflow.workflowItemTypes.Root.find({
                    active: true,
                    deleted: false
                }, function(err, workflows) {
                    logger.silly('[wf admin list] workflows: ' + util.inspect(workflows));
                    p.workflows = workflows;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //separate drafts from published
                p.drafts = _.filter(p.workflows, function(workflow) {
                    return !!workflow.isDraftOf;
                });
                p.published = _.filter(p.workflows, function(workflow) {
                    return !workflow.isDraftOf;
                });

                cb(null, models, p);
            },
            function(models, p, cb) {
                //load the published flattened
                if (p.published.length == 0) {
                    cb(null, models, p);
                } else {
                    projectUtility.definition.workflow.loader(models, {
                        actionCustomerUser: req.actionCustomerUser,
                        workflowItemDefinitions: p.published,
                        flattened: true,
                    }, function(err, models, workflows) {
                        p.published = workflows;
                        cb(err, models, p);
                    });
                }
            },
            function(models, p, cb) {
                //load the drafts unflattened
                if (p.drafts.length == 0) {
                    cb(null, models, p);
                } else {

                    projectUtility.definition.workflow.loader(models, {
                        actionCustomerUser: req.actionCustomerUser,
                        workflowItemDefinitions: p.drafts,
                        flattened: false,
                    }, function(err, models, workflows) {
                        p.drafts = workflows;
                        cb(err, models, p);
                    });
                };
            },
            function(models, p, cb) {
                //make {published, draft} tuples

                var tuples = [];
                //take care of the new and draft tuples
                tuples = tuples.concat(_.map(p.drafts, function(draft) {
                    return {
                        draft: draft,
                        published: _.find(p.published, function(published) {
                            return published._id.toString() == draft.isDraftOf.toString();
                        })
                    };
                }));
                //filter the published to the undrafted published
                //map to tuples
                tuples = tuples.concat(_.chain(p.published)
                    .filter(function(published) {
                        return !_.any(p.drafts, function(draft) {
                            return published._id.toString() == draft.isDraftOf.toString();
                        });
                    })
                    .map(function(published) {

                        return {
                            draft: null,
                            published: published
                        };
                    })
                    .value());

                p.tuples = tuples;
                cb(null, models, p);
            }
        ], function(err, models, p) {
            res.err = err;
            res.data = p.tuples;
            next();
        });
    },
    load: function(req, res, next) {
        projectUtility.definition.workflow.loader(req.models, {
            actionCustomerUser: req.customerUser,
            workflowItemDefinition: req.params.workflow
        }, function(err, models, workflow) {
            res.err = err;
            res.data = workflow;
            next();
        });
    },
    create: function(req, res, next) {
        logger.silly('about to create draft of wf');
        projectUtility.definition.workflow.draft.create(req.models, {
            customer: req.customer,
            workflowItemDefinition: req.body,
            actionCustomerUser: req.actionCustomerUser
        }, function(err, models, draft) {
            res.err = err;
            res.data = draft;
            next();
        });
    },
    loadByShortName: function(req, res, next) {
        logger.silly('loading wf shortname: ' + req.params.shortName);
        async.waterfall([

            function(cb) {
                projectUtility.definition.workflow.loadByShortName(req.models, {
                    customer: req.customer,
                    shortName: req.params.shortName
                }, function(err, models, workflows) {
                    cb(err, models, {
                        workflows: workflows
                    });
                });
            },
            function(models, p, cb) {
                //there should be zero, one or two workflows
                var published = _.filter(p.workflows, function(workflow) {
                        return !workflow.isDraftOf;
                    }),
                    drafts = _.filter(p.workflows, function(workflow) {
                        return !!workflow.isDraftOf;
                    });

                if (published.length > 1) {
                    //more than one published workflow has the same shortname.
                    res.statusOverride = 400;
                    return cb('Customer ' + util.inspect(req.customer) + ' has more than one published workflow for ' + req.params.shortName);
                }
                if (drafts.length > 1) {
                    //more than one published workflow has the same shortname.
                    res.statusOverride = 400;
                    return cb('Customer ' + util.inspect(req.customer) + ' has more than one drafted workflow for ' + req.params.shortName);
                }

                p.tuple = {
                    published: _.first(published),
                    draft: _.first(drafts)
                };
                cb(null, models, p);
            },
        ], function(err, models, p) {
            res.err = err;
            res.data = p.tuple;
            next();
        });

    },
    update: function(req, res, next) {
        projectUtility.definition.workflow.draft.update(req.models, {
            customer: req.customer,
            workflowItemDefinition: req.body, //todo: use req.body for workflow def in admin?
            actionCustomerUser: req.actionCustomerUser,
            flatten: true
        }, function(err, models, draft) {
            res.err = err;
            res.data = draft;
            next();
        })
    },
    publish: function(req, res, next) {
        projectUtility.definition.workflow.draft.publish(req.models, {
            workflowItemDefinition: req.body,
            customer: req.customer,
            actionCustomerUser: req.actionCustomerUser
        }, function(err, models, published) {
            res.err = err;
            res.data = published;
            next();
        });
    },
    close: function(req, res, next) {
        projectUtility.definition.workflow.draft.close(req.models, {
            workflowItemDefinition: req.body,
            customer: req.customer,
            actionCustomerUser: req.actionCustomerUser
        }, function(err, models, closedDraft) {
            res.err = err;
            res.data = closedDraft._id;
            next();
        });
    },
}
