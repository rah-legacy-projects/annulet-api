var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    request = require('request'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    config = require('annulet-config'),
    Mandrill = require('mandrill-api')
    .Mandrill;
require('annulet-util');
module.exports = exports = {
    list: function(req, res, next) {
        async.waterfall([

            function(cb) {
                cb(null, req.models, {})
            },
            function(models, p, cb) {
                //get the active/undeleted alerts
                models.definitions.alert.Alert.find({
                    active: true,
                    deleted: false
                }, function(err, alerts) {
                    p.alerts = alerts;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //separate drafts from published
                p.drafts = _.filter(p.alerts, function(alert) {
                    return !!alert.isDraftOf;
                });
                p.published = _.filter(p.alerts, function(alert) {
                    return !alert.isDraftOf;
                });

                cb(null, models, p);
            },
            function(models, p, cb) {
                if (p.published.length == 0) {
                    return cb(null, models, p);
                }
                //load the published flattened
                projectUtility.definition.alert.loader(models, {
                    actionCustomerUser: req.actionCustomerUser,
                    alertDefinitions: p.published,
                    flattened: true,
                }, function(err, models, alerts) {
                    p.published = alerts;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                if (p.drafts.length == 0) {
                    return cb(null, models, p);
                }
                //load the drafts unflattened
                projectUtility.definition.alert.loader(models, {
                    actionCustomerUser: req.actionCustomerUser,
                    alertDefinitions: p.drafts,
                    flattened: false,
                }, function(err, models, alerts) {
                    p.drafts = alerts;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //make {published, draft} tuples

                var tuples = [];
                //take care of the new and draft tuples
                tuples = tuples.concat(_.map(p.drafts, function(draft) {
                    return {
                        draft: draft,
                        published: _.find(p.published, function(published) {
                            return published._id.toString() == draft.isDraftOf.toString();
                        })
                    };
                }));
                //filter the published to the undrafted published
                //map to tuples
                tuples = tuples.concat(_.chain(p.published)
                    .filter(function(published) {
                        return !_.any(p.drafts, function(draft) {
                            return published._id.toString() == draft.isDraftOf.toString();
                        });
                    })
                    .map(function(published) {
                        return {
                            draft: null,
                            published: published
                        };
                    })
                    .value());

                p.tuples = tuples;
                cb(null, models, p);
            }
        ], function(err, models, p) {
            res.err = err;
            res.data = p.tuples;
            next();
        });
    },
    load: function(req, res, next) {
        projectUtility.definition.alert.loader(req.models, {
            actionCustomerUser: req.customerUser,
            alertDefinition: req.params.alert
        }, function(err, models, alert) {
            res.err = err;
            res.data = alert;
            next();
        });
    },
    loadTuple: function(req, res, next) {
        logger.silly('loading tuple: ' + req.params.alert);
        logger.silly('models: ' + !!req.models);
        projectUtility.definition.alert.tupleLoader(req.models, {
            actionCustomerUser: req.actionCustomerUser,
            alert: req.params.alert
        }, function(err, models, tuple) {
            res.err = err;
            res.data = tuple;
            logger.silly('tuple loaded.');
            next();
        });
    },
    create: function(req, res, next) {
        projectUtility.definition.alert.draft.create(req.models, {
            customer: req.customer,
            alertDefinition: req.body,
            actionCustomerUser: req.actionCustomerUser
        }, function(err, models, draft) {
            res.err = err;
            res.data = draft;
            next();
        });
    },
    update: function(req, res, next) {
        logger.silly('[alert admin controller] body: ' + util.inspect(req.body));
        projectUtility.definition.alert.draft.update(req.models, {
            customer: req.customer,
            alertDefinition: req.body, //todo: use req.body for alert def in admin?
            actionCustomerUser: req.actionCustomerUser,
            flatten: true
        }, function(err, models, draft) {
            res.err = err;
            res.data = draft;
            next();
        });
    },
    publish: function(req, res, next) {
        projectUtility.definition.alert.draft.publish(req.models, {
            alertDefinition: req.body,
            customer: req.customer,
            actionCustomerUser: req.actionCustomerUser
        }, function(err, models, published) {
            res.err = err;
            res.data = published;
            next();
        });
    },
    close: function(req, res, next) {
        projectUtility.definition.alert.draft.close(req.models, {
            alertDefinition: req.body,
            customer: req.customer,
            actionCustomerUser: req.actionCustomerUser
        }, function(err, models, closedDraft) {
            res.err = err;
            res.data = closedDraft._id;
            next();
        });

    },
}
