var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    request = require('request'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    config = require('annulet-config'),
    Mandrill = require('mandrill-api')
    .Mandrill;
require('annulet-util');
module.exports = exports = {
    getUsers: function(req, res, next) {
        //req.customer comes from the customer validity middleware
        async.waterfall([

            function(cb) {
                cb(null, req.models);
            },

            function(models, cb) {
                logger.silly('[customer api] getting customer: ' + req.customer);
                //get the customer
                models.Customer.findOne({
                    _id: req.customer.toObjectId()
                })
                    .exec(function(err, customer) {
                        cb(err, models, {
                            customer: customer
                        });
                    });
            },
            function(models, p, cb) {
                logger.silly('[customer api] getting users');
                //get the users
                models.auth.CustomerUser.find({
                    deleted: false,
                    customer: req.customer
                })
                    .exec(function(err, customerUsers) {
                        p.customerUsers = customerUsers;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                logger.silly('[customer api] getting permissions for each user');
                //get the users' permissions on the customer
                async.parallel(_.map(p.customerUsers, function(customerUser) {
                    return function(cb) {
                        var access = p.customer.getAccess(customerUser);
                        cb(null, {
                            customerUser: customerUser,
                            access: access
                        });
                    }
                }), function(err, r) {
                    //map the user/access pairs to something the UI can consume
                    var users = _.chain(r)
                        .filter(function(rs) {
                            return !_.any(rs.access, function(a) {
                                return /system user/i.test(a);
                            });
                        })
                        .map(function(rs) {
                            return {
                                //firstName: rs.user.firstName,
                                //lastName: rs.user.lastName,
                                //email: rs.user.email,
                                access: rs.access,
                                _id: rs.customerUser._id,
                                userId: rs.customerUser.user
                            };
                        })
                        .value();

                    p.refinedUsers = users;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                logger.silly('[customer api] reaching out to ID API to get extended user information');
                //issue a request to the ID service to get user information
                async.parallel(_.map(p.refinedUsers, function(refinedUser) {
                    return function(cb) {
                        var url = config.configuration.paths.authUri() + '/user/get/' + refinedUser.userId;
                        logger.silly('url: ' + url);
                        request.get({
                            url: url,
                            headers: {
                                'annulet-internal': config.configuration.paths.internalKey()
                            }
                        }, function(err, response, body) {
                            logger.silly(body);
                            var userInformation = JSON.parse(body);
                            cb(err, _.merge(refinedUser, userInformation.data));
                        })
                    }
                }), function(err, r) {
                    p.refinedUsers = r;
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            logger.silly('[customer api] finished getting user listing');
            res.err = err;
            res.data = p.refinedUsers;
            next();
        });
    },
    getUser: function(req, res, next) {
        async.waterfall([

            function(cb) {
                cb(null, req.models);
            },
            function(models, cb) {
                //get the customer
                models.Customer.findOne({
                    _id: req.customer.toObjectId()
                })
                    .exec(function(err, customer) {
                        cb(err, models, {
                            customer: customer
                        });
                    });
            },
            function(models, p, cb) {
                logger.silly('trying to find customer user ' + req.params.customerUser + ' (' + req.params.customerUser.length + ')');

                if (!/^[0-9a-fA-F]{24}$/.test(req.params.customerUser)) {
                    res.statusOverride = 404;
                    return cb("User not found for edit: " + req.params.customerUser);
                }

                models.auth.CustomerUser.findOne({
                    //customer: p.customer._id,
                    //_id: req.params.customerUser.toObjectId()
                    user: req.params.customerUser.toObjectId()
                    //deleted: false
                })
                    .exec(function(err, customerUser) {
                        if (!!err) {
                            logger.error('problem getting user: ' + util.inspect(err))
                        }
                        if (!customerUser) {
                            res.statusOverride = 404;
                            return cb("User not found for edit: " + req.params.customerUser);
                        }
                        p.customerUser = customerUser;
                        p.access = p.customer.getAccess(customerUser);
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                request.get({
                    url: config.configuration.paths.authUri() + '/user/get/' + p.customerUser.user,
                    headers: {
                        'annulet-internal': config.configuration.paths.internalKey()
                    }
                }, function(err, response, body) {
                    var userInformation = JSON.parse(body);
                    p.modifiedUser = _.merge(p.customerUser.toObject(), userInformation.data);
                    p.modifiedUser.access = p.access;
                    cb(err, models, p);
                })
            }
        ], function(err, models, p) {
            res.err = err;
            if (!!err) {
                logger.error(util.inspect(err));
            }
            if (!!p) {
                res.data = p.modifiedUser;
            }
            next();
        });
    },
    saveUser: function(req, res, next) {
        logger.silly('[user save] starting to save...');
        async.waterfall([

            function(cb) {
                cb(null, req.models);
            },
            function(models, cb) {
                logger.silly('[user save] getting customer');
                models.Customer.findOne({
                    _id: req.customer.toObjectId()
                })
                    .exec(function(err, customer) {
                        cb(err, models, {
                            customer: customer
                        });
                    });
            },
            function(models, p, cb) {
                logger.silly('[user save] reaching out to ID service...');
                //ask the ID service if this email exists
                request.get({
                    json: true,
                    url: config.configuration.paths.authUri() + '/user/get/' + req.body.user.email,
                    headers: {
                        'annulet-internal': config.configuration.paths.internalKey()
                    }
                }, function(err, response, body) {
                    p.user = body.data;
                    cb(err, models, p);
                });

            },
            function(models, p, cb) {
                //check to see if a user for this customer already exists for the agnostic user
                if (!req.body.user._id) {
                    models.auth.CustomerUser.findOne({
                        user: p.user._id
                    })
                        .exec(function(err, customerUser) {
                            if (!customerUser) {
                                logger.silly('new user is actually new, carry on');
                                cb(err, models, p);
                            } else {
                                p.customerUser = customerUser;
                                if (!p.customerUser.active || p.customerUser.deleted) {
                                    logger.silly('user found for recreation but was inactive, saving as that user');
                                    return cb(err, models, p);
                                }
                                logger.warn('trying to recreate existing user...');
                                cb({
                                    recreation: true
                                }, models, p);
                            }
                        });
                } else {
                    cb(null, models, p);
                }
            },
            function(models, p, cb) {
                logger.silly('[save user] getting customer user');
                //get the customer
                models.auth.CustomerUser.findOne({
                    _id: (!!req.body.user._id ? req.body.user._id.toObjectId() : null)
                })
                    .exec(function(err, customerUser) {
                        p.customerUser = customerUser;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                var internal = req.get('annulet-internal') || req.query['annulet_internal'];
                if (!!internal) {
                    //if this is an internal request, use system as the logged in user
                    logger.silly('[save user] inernal request, using sys internal as modifying user');
                    p.loggedInCustomerUser = {
                        _id: '[system internal]'
                    };
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                logger.silly('[save user] saving agnostic user');
                //post the user information to the ID service - manages customer-agnostic info
                request.post({
                    json: true,
                    body: req.body,
                    url: config.configuration.paths.authUri() + '/user/save/' + (p.customerUser || {
                        user: 'new'
                    })
                        .user,
                    headers: {
                        'annulet-internal': config.configuration.paths.internalKey()
                    }
                }, function(err, response, body) {
                    p.user = body.data;
                    cb(err, models, p);
                });
            },

            function(models, p, cb) {
                //hack: clone the body's user
                //hack: add the federal employee class
                var customerUser = _.clone(req.body.user);
                customerUser.employeeClass = customerUser.employeeClass || [];
                if (!_.any(customerUser.employeeClass, function(c) {
                    return c == 'Federal'
                })) {
                    customerUser.employeeClass.push('Federal');
                }
                logger.silly('[save user] managing local customer user');
                projectUtility.auth.manageCustomerUser(models, {
                    actionCustomerUser: req.actionCustomerUser,
                    user: p.user._id,
                    customer: p.customer._id,
                    roles: req.body.roles,
                    customerUser: customerUser,
                }, function(err, models, customerUser) {
                    p.customerUser = customerUser;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                var tasks = [

                    function(cb) {
                        cb(null, {});
                    }
                ];
                if (p.user.isNew) {
                    //add an activation on the id service
                    tasks.push(function(pp, cb) {
                        logger.silly('[user save] user is new, adding activation');
                        request.post({
                            json: true,
                            body: req.body.user,
                            url: config.configuration.paths.authUri() + '/activation',
                            headers: {
                                'annulet-internal': config.configuration.paths.internalKey()
                            },
                        }, function(err, response, body) {
                            pp.activation = body.data;
                            cb(err, pp);
                        });
                    });
                }
                if (p.customerUser.isNew) {
                    //queue welcome email w/ customer specifics
                    tasks.push(function(pp, cb) {
                        logger.silly('[user save] adding mandrill email');
                        mandrillClient = new Mandrill(config.configuration.mandrill.privateKey());
                        var mandrillOptions = {
                            template_name: 'Customer User Welcome',
                            template_content: {},
                            message: {
                                'to': [{
                                    'email': req.body.user.email,
                                    'name': req.body.user.firstName + ' ' + req.body.user.lastName,
                                    'type': 'to'
                                }],
                                merge_vars: [{
                                    rcpt: req.body.user.email,
                                    vars: [{
                                        name: 'CUSTOMER_NAME',
                                        content: p.customer.name
                                    }, {
                                        name: 'LOGIN_LINK',
                                        content: config.configuration.paths.uiUri() + '/login'
                                    }],
                                }],
                            }
                        };
                        if (pp.activation) {
                            logger.silly('[user save] sending activation email');
                            mandrillOptions.message.merge_vars[0].vars.push({
                                name: 'ACTIVATION_LINK',
                                content: config.configuration.paths.uiUri() + '/activation/' + pp.activation.slug
                            });
                            mandrillOptions.template_name = 'Customer User Activation';
                        }

                        mandrillClient.messages.sendTemplate(mandrillOptions, function(result) {
                            //email sent
                            cb(null, pp);
                        }, function(error) {
                            logger.error('[save user] problem with email via mandrill: ' + util.inspect(error));
                            cb(error, pp);
                        });
                    });
                }

                async.waterfall(tasks, function(err, pp) {
                    logger.silly('[save user] tasks: ' + tasks.length + ' ran for user email/activation.');
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            logger.silly('[save user] done.');
            //hack: make object out of customer user
            if (!!p.customerUser.toObject) {
                p.customerUser = p.customerUser.toObject();
            }
            //hack: put the recreation problem on the customerUser so the error isn't caught by jquery error
            if (!!err && !err.recreation) {
                logger.error(util.inspect(err));
                res.err = err;
            } else if (!!err && !!err.recreation) {
                logger.warn('customer user was attempted to recreate, sending that back');
                p.customerUser.recreation = true;
            }

            logger.silly('[save user] err; ' + util.inspect(err));
            res.data = p.customerUser;
            next();
        });
    },
    removeUser: function(req, res, next) {
        async.waterfall([

            function(cb) {
                cb(null, req.models);
            },
            function(models, cb) {
                models.auth.CustomerUser.findOne({
                    user: (!!req.body.user._id ? req.body.user._id.toObjectId() : null),
                    customer: req.customer.toObjectId()
                })
                    .exec(function(err, user) {
                        cb(err, models, {
                            user: user
                        });
                    });
            },
            function(models, p, cb) {
                p.user.modifiedBy = req.actionCustomerUser._id.toString();
                p.user.active = false;
                p.user.deleted = true;
                p.user.save(function(err, user) {
                    p.user = user;
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            res.err = err;
            res.data = null;
            next();
        });
    }
}
