var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util;

require('annulet-util');

module.exports = exports = {
    get: function(req, res, next) {
        async.waterfall([

            function(cb) {
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                //is this a def or instance?
                async.parallel({
                    definition: function(cb) {
                        models.definitions.workflow.workflowItemTypes.Root.findOne({
                            $or: [{
                                _id: req.params.workflowItem.toObjectId()
                            }, {
                                shortName: req.params.workflowItem
                            }]
                        })
                            .exec(function(err, root) {
                                cb(err, (root || {})
                                    ._id);
                            });
                    },
                    instance: function(cb) {
                        models.instances.workflow.workflowItemTypes.Root.findOne({
                            _id: req.params.workflowItem.toObjectId()
                        })
                            .exec(function(err, root) {
                                cb(err, (root || {})
                                    .itemDefinition);
                            });
                    }
                }, function(err, r) {
                    p.workflowItemToStamp = r.instance || r.definition;
                    logger.silly('[wf get] item to stamp: ' + p.workflowItemToStamp);
                    if(!p.workflowItemToStamp){
                        res.statusOverride = 404;
                        return cb('Could not find workflow for ' + req.params.workflowItem, models, p);
                    }
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //stamp the definition ID retrieved
                projectUtility.definition.workflow.stamper.stamper(models, {
                    workflowItemDefinition: p.workflowItemToStamp,
                    actionCustomerUser: req.actionCustomerUser,
                    customerUser: req.actionCustomerUser,
                    flatten: true
                }, function(err, models, stamped) {
                    p.wfRoot = stamped;
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            res.err = err;
            res.data = p.wfRoot;
            next();
        });
    },
    list: function(req, res, next) {
        async.waterfall([

            function(cb) {
                logger.silly('[get workflows] about to get workflows');
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                projectUtility.instance.workflow.status(models, {
                    actionCustomerUser: req.actionCustomerUser,
                    customerUser: req.actionCustomerUser
                }, function(err, models, statuslist) {
                    p.workflows = statuslist;
                    cb(err, models, p);
                });
            },
        ], function(err, models, p) {
            logger.silly('[get workflows] done');
            logger.silly('[get workflows] retrieved ' + p.workflows.length);
            if (!!err) {
                logger.error(util.inspect(err))
            }

            res.err = err;
            res.data = p.workflows;
            next();
        });
    }
};
