var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    stripe = require('stripe')(require('annulet-config')
        .configuration.stripe.privateKey()),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    request = require('request'),
    config = require('annulet-config'),
    Mandrill = require('mandrill-api')
    .Mandrill,
    content = require('annulet-content'),
    request = require('request');

module.exports = exports = {
    createCustomer: function(req, res, next) {
        async.waterfall([

            function(cb) {
                logger.silly('[sign up] creating customer');
                //create the customer
                projectUtility.customer.create({
                    customer: req.body.customer
                }, function(err, models, customer) {
                    cb(err, models, {
                        customer: customer
                    });
                });
            },
            function(models, p, cb) {
                if (/automatedTest/.test(process.env.ENV)) {
                    p.stripeCustomer = {
                        id: config.configuration.stripe.sampleCustomerId()
                    };
                    cb(null, models, p);
                } else {
                    //create stripe customer
                    //todo: plan selection
                    logger.silly('[sign up] adding stripe customer');
                    logger.silly('[sign up] stripe id: ' + req.body.stripe.id);
                    stripe.customers.create({
                        source: req.body.stripe.id,
                        description: p.customer.name,
                        plan: 'basic',
                        email: req.body.user.email,
                    }, function(err, stripeCustomer) {
                        p.stripeCustomer = stripeCustomer;
                        cb(err, models, p);

                    });
                }
            },
            function(models, p, cb) {
                //save the stripe customer ID in the customer
                logger.silly('[sign up] adding stripe customer id to customer');
                logger.silly(p.stripeCustomer.id);
                p.customer.stripeCustomerId = p.stripeCustomer.id;
                p.customer.save(function(err, customer) {
                    p.customer = customer;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //create sudoer for customer
                request.post({
                    json: true,
                    url: config.configuration.paths.apiUri() + '/admin/userManagement/save',
                    headers: {
                        'annulet-internal': config.configuration.paths.internalKey(),
                        'annulet-auth-customer': p.customer._id
                    },
                    body: {
                        user: config.configuration.administrativeUser(),
                        customer: p.customer._id,
                        roles: ['Owner', 'Administrator', 'User', 'System User']
                    }
                }, function(err, response, body) {
                    p.user = body.data;
                    cb(err, models, p)
                });
            },
            function(models, p, cb) {
                content.customer({
                    customer: p.customer._id
                }, function(err, contentPackage) {
                    p.contentPackage = contentPackage;
                    cb(err, models, p);
                });
            },

            function(models, p, cb) {
                //create the (actual) owner

                logger.silly('[sign up] creating user');
                logger.silly('[sign up] granting user owner');

                var body = {
                    user: req.body.user,
                    customer: p.customer._id,
                    roles: ['Owner']
                };

                request.post({
                    json: true,
                    body: body,
                    url: config.configuration.paths.apiUri() + '/admin/userManagement/save',
                    headers: {
                        'annulet-internal': config.configuration.paths.internalKey(),
                        'annulet-auth-customer': p.customer._id
                    }
                }, function(err, response, responseBody) {
                    p.user = responseBody.data;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //queue welcome email
                logger.silly('[sign up] queueing welcome email');
                var mandrillClient = new Mandrill(config.configuration.mandrill.privateKey());
                mandrillClient.messages.sendTemplate({
                    template_name: 'Welcome',
                    template_content: {},
                    message: {
                        to: [{
                            email: req.body.user.email,
                            name: req.body.user.firstName + ' ' + req.body.user.lastName,
                            type: 'to'
                        }]
                    }
                }, function(result) {
                    logger.info('welcome email sent to ' + req.body.user.email);
                    cb(null, models, p);
                }, function(error) {
                    logger.error('Problem sending mandrill email: ' + util.inspect(error));
                    cb(error, models, p);
                })
            }
        ], function(err, models, p) {
            if (!!err) {
                logger.error('[sign up] error: ' + util.inspect(err));
            } else {
                logger.silly('[sign up] complete.');
            }
            res.err = err;
            //hack: reduce the signup data to customer ID and ... reduced user data?
            res.data = {
                customer: {
                    _id: p.customer._id
                },
                user: {
                    _id: p.user.user
                }
            };
            next();
        });
    }
};
