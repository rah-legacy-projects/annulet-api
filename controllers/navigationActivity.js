var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    moment = require('moment');

module.exports = exports = {
    make: function(req, res) {
        logger.silly('[activity] making.');
        async.waterfall([

            function(cb) {
                logger.silly('[activity] getting models, customer ' + req.body.customer);
                if (/^[0-9a-fA-F]{24}$/.test(req.body.customer)) {
                    modeler.db({
                        collection: 'Customer',
                        query: {
                            customer: req.body.customer.toObjectId()
                        }
                    }, function(err, models) {
                        cb(err, models, {});
                    });
                } else {
                    modeler.db(null, function(err, models) {
                        logger.warn('putting activity on default server');
                        cb(err, models, {});
                    });
                }
            },
            function(models, p, cb) {
                try {
                    (new models.tracking.NavigationActivity(req.body))
                        .save(function(err, activity) {
                            logger.silly('[activity] activity saved.');
                            p.activity = activity;
                            cb(err, models, p);
                        });
                } catch (err) {
                    logger.error('!!! problem making activity: ' + util.inspect(err));
                    logger.error('eating exeption.');
                    res.status(200)
                        .json({});
                }
            },
        ], function(err, models, p) {
            logger.silly('[activity] finished');
            res.status(200)
                .json({});
        });

    }
};
