var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    Busboy = require('busboy'),
    moment = require('moment'),
    mime = require('mime'),
    path = require('path');

module.exports = exports = {
    list: function(req, res, next) {

        async.waterfall([

            function(cb) {
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                projectUtility.definition.auxiliaryDocument.stamper.stampByCustomerUser(models, {
                    customerUser: req.actionCustomerUser,
                    actionCustomerUser: req.actionCustomerUser,
                    flatten: true
                }, function(err, models, auxdox) {
                    logger.silly('auxdox: ' + util.inspect(auxdox));
                    p.auxiliaryDocuments = auxdox;
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            res.data = p.auxiliaryDocuments;
            res.err = err;
            next();
        });
    },
    recent: function(req, res, next) {
        async.waterfall([

            function(cb) {
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                models.definitions.auxiliaryDocument.AuxiliaryDocument.find({
                    active: true,
                    deleted: false,
                    modified: {
                        $gte: moment()
                            .subtract(7, 'days')
                    }
                })
                    .sort('-modified')
                    .exec(function(err, documents) {
                        cb(err, models, {
                            documents: documents
                        });
                    });
            }
        ], function(err, models, p) {
            logger.silly('[recent auxdox] documents: ' + p.documents.length);
            var howManyMore = p.documents.length - 5;
            if (howManyMore < 0) {
                howManyMore = 0;
            }
            res.data = {
                howManyMore: howManyMore,
                documents: p.documents.slice(0, 5)
            };
            res.err = err;
            next();
        });
    },
    getFile: function(req, res) {
        async.waterfall([

            function(cb) {
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                models.auth.CustomerUser.findOne({
                    user: req.loggedInUser._id.toObjectId(),
                    customer: req.customer.toObjectId()
                })
                    .exec(function(err, customerUser) {
                        logger.silly('[getfile] got customer user');
                        p.customerUser = customerUser;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //get the aux doc
                projectUtility.instance.auxiliaryDocument.loader(models, {
                    auxiliaryDocumentInstance: req.params.file.toObjectId(),
                    actionCustomerUser: req.actionCustomerUser,
                    flatten:true
                }, function(err, models, instance){
                    p.auxiliaryDocument = instance;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                logger.silly('[getfile] looking for customer ' + req.customer + ' for file ' + p.auxiliaryDocument.file);
                models.connection.db.collection('C' + req.customer + '.files', function(err, collection) {
                    if (!!err) {
                        logger.error('[getfile] problem making collection: ' + util.inspect(err));
                        return cb(err);
                    }
                    logger.silly('[getfile] customer collection made');
                    collection.find({
                        _id: p.auxiliaryDocument.file
                    })
                        .toArray(function(err, document) {
                            p.file = document[0];
                            cb(err, models, p);
                        });
                });
            },
            function(models, p, cb) {
                //add a view for the logged in user
                models.instances.auxiliaryDocument.ranged.AuxiliaryDocument.findOne(p.auxiliaryDocument._rangeId).exec(function(err, auxiliaryDocumentRange){
                    auxiliaryDocumentRange.views++;
                    auxiliaryDocumentRange.lastViewed = moment().toDate();
                    auxiliaryDocumentRange.save(function(err, auxiliaryDocumentRange){
                        p.auxiliaryDocumentRange = auxiliaryDocumentRange;
                        cb(err, models, p);
                    });
                });
            },
        ], function(err, models, p) {
            var rs = models.grid.createReadStream({
                _id: p.auxiliaryDocument.file,
                root: 'C' + req.customer
            });

            var mimetype = mime.lookup(p.file.filename);
            var extname = path.extname(p.auxiliaryDocument.displayName);

            //clean the display name, including only valid file characters
            var cleanedDisplayName = p.auxiliaryDocument.displayName.replace(/[^a-zA-Z0-9\.,\-\_ ]/g, '_');
            var basename = path.basename(cleanedDisplayName, extname);
            var dlname = basename + '.' + mime.extension(mimetype);
            logger.silly('cleaned display name:' + cleanedDisplayName);

            res.setHeader('Content-Disposition', 'attachment;filename=' + dlname);
            res.writeHead(200, {
                'Content-Type': p.file.contentType,
                'Content-Length': p.file.length
            });

            rs.pipe(res);
        });
    },
};
