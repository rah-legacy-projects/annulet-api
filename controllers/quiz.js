var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    moment = require('moment');
require('annulet-util');
module.exports = exports = {
    get: function(req, res, next) {
        async.waterfall([

            function(cb) {
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                //is this a def or instance?
                async.parallel({
                    definition: function(cb) {
                        models.definitions.quiz.Quiz.findOne({
                            $or: [{
                                _id: req.params.quiz.toObjectId()
                            }, {
                                shortName: req.params.quiz
                            }]
                        })
                            .exec(function(err, op) {
                                cb(err, (op || {})
                                    ._id);
                            });
                    },
                    instance: function(cb) {
                        models.instances.quiz.Quiz.findOne({
                            _id: req.params.quiz.toObjectId()
                        })
                            .exec(function(err, op) {
                                cb(err, (op || {})
                                    .quizDefinition);
                            });
                    }
                }, function(err, r) {
                    p.quizToStamp = r.instance || r.definition;
                    p.definition = r.definition;
                    if (!p.quizToStamp) {
                        res.statusOverride = 404;
                        return cb('Could not find quiz for ' + req.params.quiz, models, p);
                    }
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //stamp the definition ID retrieved
                projectUtility.definition.quiz.stamper.stamper(models, {
                    quizDefinition: p.quizToStamp,
                    actionCustomerUser: req.actionCustomerUser,
                    customerUser: req.actionCustomerUser,
                    flatten: true
                }, function(err, models, stamped) {
                    p.attempt = stamped;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                logger.silly('[quiz get] quiz def wf item: ' + util.inspect(p.definition));
                //get the quiz wf def ranges
                models.definitions.workflow.ranged.workflowItemTypes.Quiz.find({
                    quiz: p.definition
                })
                    .exec(function(err, quizWorkflowDefRanges) {
                        logger.silly('[quiz get] def ranges: ' + quizWorkflowDefRanges.length);
                        p.quizWorkflowDefRanges = quizWorkflowDefRanges;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //get the items those ranges correspond to
                models.definitions.workflow.workflowItemTypes.Quiz.find({
                    rangedData: {
                        $elemMatch: {
                            $in: _.pluck(p.quizWorkflowDefRanges, '_id')
                        }
                    }
                })
                    .exec(function(err, quizWorkflowDefs) {
                        logger.silly('[quiz get] defs: ' + quizWorkflowDefs.length);
                        p.quizWorkflowDefs = quizWorkflowDefs;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //get the root of those defs
                async.parallel(_.map(p.quizWorkflowDefs, function(quizWorkflowDef) {
                    return function(cb) {
                        projectUtility.definition.workflow.rootForItem(models, {
                            workflowItem: quizWorkflowDef
                        }, function(err, models, rootDef) {
                            cb(err, rootDef);
                        });
                    };
                }), function(err, r) {
                    logger.silly('[quiz get] got root defs: ' + r.length);
                    p.rootDefinitions = r;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //get the instances for the user for the def roots
                models.instances.workflow.workflowItemTypes.Root.find({
                    $and: [{
                        _id: {
                            $in: req.actionCustomerUser.workflowInstances
                        }
                    }, {
                        itemDefinition: {
                            $in: _.pluck(p.rootDefinitions, '_id')
                        }
                    }]
                })
                    .exec(function(err, rootInstances) {
                        logger.silly('[quiz get] got root instances: ' + rootInstances.length);
                        p.rootInstances = rootInstances;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //load those wf instances
                async.parallel(_.map(p.rootInstances, function(rootInstance) {
                    return function(cb) {
                        projectUtility.instance.workflow.loader(models, {
                            workflowItemInstance: rootInstance,
                            flatten: true
                        }, function(err, models, wfInstance) {
                            cb(err, wfInstance);
                        });
                    };
                }), function(err, r) {
                    logger.silly('[quiz get] got wf instances from loader: ' + r.length);
                    p.workflowInstances = r;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //find the quiz item instances for the quiz item defs
                var instanceRanges = _.chain(p.workflowInstances)
                    .map(function(workflowInstance) {
                        return _.filterDeep(workflowInstance, {
                            itemDefinitionRange: function(key, value) {
                                return _.any(p.quizWorkflowDefRanges, function(defRange) {
                                    return defRange._id.toString() == (value || '')
                                        .toString();
                                });
                            }
                        });
                    })
                    .reduce(function(a, b) {
                        return a.concat(b);
                    }, [])
                    .compact()
                    .value();

                logger.silly('[quiz get] got instance ranges, about to save: ' + instanceRanges.length);

                var instanceRangeIds = _.map(instanceRanges, function(instanceRange) {
                    return instanceRange._rangeId; //.toObjectId();
                });

                //load the instances ranges
                models.instances.workflow.ranged.workflowItemTypes.Quiz.find({
                    _id: {
                        $in: instanceRangeIds
                    }
                })
                    .exec(function(err, quizInstanceRanges) {
                        logger.silly('[quiz get] quiz instance ranges: ' + quizInstanceRanges.length);
                        p.quizInstanceRanges = quizInstanceRanges;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //find the quiz instance for the attempt
                models.instances.quiz.Quiz.findOne({
                    attempts: p.attempt._id
                })
                    .exec(function(err, quiz) {
                        p.quiz = quiz;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //save the stamped quiz in those instances
                async.parallel(_.map(p.quizInstanceRanges, function(range) {
                    return function(cb) {
                        range.quiz = p.quiz._id;
                        range.save(function(err, range) {
                            cb(err, range);
                        });
                    };
                }), function(err, r) {
                    logger.silly('[quiz get] ranges saved: ' + r.length);
                    logger.silly('[quiz get] ranges: ' + util.inspect(r));
                    logger.silly('[quiz get] quiz id: ' + p.quiz._id);
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            res.err = err;
            res.data = p.attempt;
            next();
        });
    },
    getSummary: function(req, res, next) {
        logger.silly('[quiz summary] looking for ' + req.params.attempt);
        if (!/^[0-9a-fA-F]{24}$/.test(req.params.attempt)) {
            res.statusOverride = 404;
            res.err = ("Invalid attempt ID.");
            return next();
        }
        async.waterfall([

            function(cb) {
                //get the models for the quiz
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                projectUtility.definition.quiz.stamper.attempt.loader(models, {
                    attempt: req.params.attempt
                }, function(err, models, attempt){
                    p.attempt = attempt;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //find the quiz instance for the attempt
                models.instances.quiz.Quiz.findOne({
                    attempts: req.params.attempt.toObjectId()
                })
                    .exec(function(err, quiz) {
                        logger.silly('[quiz summary] quiz from attempt: ' + !!quiz);
                        p.quiz = quiz;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //find the workflow item range that the quiz belongs to
                models.instances.workflow.ranged.WorkflowItem.findOne({
                    quiz: p.quiz._id
                })
                    .exec(function(err, workflowItemRange) {
                        p.workflowItemRange = workflowItemRange;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //find the workflow item that the range belongs to
                models.instances.workflow.WorkflowItem.findOne({
                    rangedData: p.workflowItemRange._id
                })
                    .exec(function(err, workflowItem) {
                        p.workflowItem = workflowItem;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //get the root item for the workflow
                projectUtility.instance.workflow.rootForItem(models, {
                    workflowItem: p.workflowItem._id
                }, function(err, models, rootItem) {
                    p.rootItem = rootItem;
                    cb(err, models, p);
                });
            },
        ], function(err, models, p) {
            if (!!err) {
                res.err = err;
                return next();
            }
            p.attempt.score(function(err, scores) {
                logger.silly('[quiz summary] scores: ' + util.inspect(scores));
                //hack: remap scores to only include scores, quiz title
                var data = {
                    scores: scores.questionScores,
                    overallScore: scores.overallScore,
                    quizTitle: p.attempt.quizDefinitionRange.title,
                    passingPercentage: p.attempt.quizDefinitionRange.passingPercentage,
                    workflowItem: p.rootItem._id
                };
                res.err = err;
                res.data = data;
                next();
            });
        });
    },

    saveAnswer: function(req, res, next) {
        async.waterfall([

            function(cb) {
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                models.instances.quiz.questionTypes[req.params.questionType].findOne({
                    _id: req.params.question.toObjectId()
                })
                    .exec(function(err, question) {
                        question.modifiedBy = req.actionCustomerUser._id.toString();
                        question.saveAnswers(req.body.answers, function(err) {
                            if (!!err) {
                                logger.error('problem saving answer: ' + util.inspect(err));
                            }
                            cb(err, question);
                        });
                    });
            }
        ], function(err, r) {
            logger.silly('done saving answer ' + req.params.questionType + ' (' + req.params.question + ')');
            if (!!err) {
                logger.error(req.params.questionType + ' (' + req.params.question + ') problem saving answer: ' + util.inspect(err));
            }

            res.err = err;
            res.data = r;
            next();
        });
    },
    finalize: function(req, res, next) {
        logger.silly('[quiz finalize] attempt: ' + req.params.attempt);
        async.waterfall([

                function(cb) {
                    cb(null, req.models, {});
                },
                function(models, p, cb) {
                    //get the attempt
                    models.instances.quiz.ranged.Attempt.findOne({
                        _id: req.params.attempt.toObjectId()
                    }, function(err, attempt) {
                        logger.silly('[quiz finalize] attempt found: ' + !!attempt);
                        p.attempt = attempt;
                        cb(err, models, p);
                    });
                },
                function(models, p, cb) {
                    //load the quiz def range
                    models.instances.quiz.ranged.Attempt.populate(p.attempt, {
                        model: models.definitions.quiz.ranged.Quiz,
                        path: 'quizDefinitionRange'
                    }, function(err, attempt) {
                        logger.silly('[quiz finalize] attempt qdr loaded: ' + !!attempt);
                        p.attempt = attempt;
                        cb(err, models, p);
                    });
                },
                function(models, p, cb) {
                    models.instances.quiz.ranged.Attempt.populate(p.attempt, {
                        model: models.instances.quiz.Question,
                        path: 'questions',
                    }, function(err, attempt) {
                        logger.silly('[quiz finalize] attempt questions loaded: ' + !!attempt);
                        p.attempt = attempt;
                        cb(err, models, p);
                    });
                },
                function(models, p, cb) {
                    //ensure completion
                    p.attempt.finalizedDate = moment()
                        .toDate();
                    p.attempt.score(function(err, scores) {
                        p.scores = scores;
                        cb(err, models, p);
                    });
                },
                function(models, p, cb) {
                    //finalize the attempt
                    p.attempt.modifiedBy = req.actionCustomerUser._id;
                    p.attempt.finalizedDate = moment()
                        .toDate();
                    p.attempt.save(function(err, attempt) {
                        p.attempt = attempt;
                        cb(err, models, p);
                    });
                },
                function(models, p, cb) {
                    //find the quiz instance for the attempt
                    models.instances.quiz.Quiz.findOne({
                        attempts: req.params.attempt.toObjectId()
                    })
                        .exec(function(err, quiz) {
                            p.quiz = quiz;
                            cb(err, models, p);
                        });
                },
                function(models, p, cb) {
                    //find the workflow item range that the quiz belongs to
                    models.instances.workflow.ranged.WorkflowItem.findOne({
                        quiz: p.quiz._id
                    })
                        .exec(function(err, workflowItemRange) {
                            p.workflowItemRange = workflowItemRange;
                            cb(err, models, p);
                        });
                },
                function(models, p, cb) {
                    //find the workflow item that the range belongs to
                    models.instances.workflow.WorkflowItem.findOne({
                        rangedData: p.workflowItemRange._id
                    })
                        .exec(function(err, workflowItem) {
                            p.workflowItem = workflowItem;
                            cb(err, models, p);
                        });
                },
                function(models, p, cb) {
                    //finalize the workflow item
                    logger.silly('[quiz summary] scores: ' + util.inspect(p.scores));
                    if (p.scores.passing) {
                        logger.silly('[quiz summary] updating wf item to be complete');
                        p.workflowItemRange.completedDate = moment()
                            .toDate();
                        p.workflowItemRange.modifiedBy = req.actionCustomerUser._id.toString();
                        p.workflowItemRange.save(function(err, workflowItemRange) {
                            logger.silly('[quiz summary] wf item saved: ' + util.inspect(workflowItemRange));
                            p.workflowItemRange = workflowItemRange;
                            cb(err, models, p);
                        });
                    } else {
                        cb(null, models, p);
                    }
                },
                function(models, p, cb) {
                    //get the root item for the workflow
                    projectUtility.instance.workflow.rootForItem(models, {
                        workflowItem: p.workflowItem._id
                    }, function(err, models, rootItem) {
                        p.rootItem = rootItem;
                        cb(err, models, p);
                    });
                },
                function(models, p, cb) {
                    (new models.tracking.ItemActivity({
                        changedBy: req.actionCustomerUser._id.toString(),
                        state: p.completeWorkflowItem ? 'Passed' : 'Failed',
                        itemType: 'instances.quiz.ranged.Attempt',
                        item: p.attempt._id,
                        message: {
                            firstName: req.loggedInUser.firstName,
                            lastName: req.loggedInUser.lastName,
                            email: req.loggedInUser.email,
                            itemTitle: p.quiz.title
                        }
                    }))
                        .save(function(err, r) {
                            cb(err, models, p);
                        });

                }
            ],
            function(err, models, p) {
                res.err = err;
                logger.silly('[quiz finalize] finalization complete');
                if (!!err) {
                    logger.error('[quiz finalize] error finalizing: ' + util.inspect(err));
                } else {
                    res.data = {
                        workflowItem: p.rootItem._id
                    };
                }
                next();
            });
    }
};
