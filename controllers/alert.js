var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    moment = require('moment');

module.exports = exports = {
    list: function(req, res, next) {
        async.waterfall([

            function(cb) {
                    cb(null, req.models, {});
            },
            function(models, p, cb) {
                if(req.params.includeDismissed === undefined){
                    req.params.includeDismissed = false;
                }
                projectUtility.definition.alert.stamper.stampByCustomerUser(models, {
                    actionCustomerUser: req.actionCustomerUser,
                    customerUser: req.actionCustomerUser,
                    customer: req.customer,
                    flatten:true,
                    includeDismissed: req.params.includeDismissed
                }, function(err, models, alerts) {
                    p.alerts = alerts;
                    cb(err, models, p);
                })
            },
        ], function(err, models, p) {
            if (!!err) {
                logger.error(util.inspect(err));
            }
            res.data = p.alerts;
            res.err = err;
            next();
        });
    },
    dismiss: function(req, res, next) {
        async.waterfall([

            function(cb) {
                    cb(null, req.models, {});
            },
            function(models, p, cb) {
                projectUtility.instance.alert.dismiss(models, {
                    alertRange: req.params.alertRange.toObjectId(),
                    customerUser: req.customerUser
                }, function(err, models, alert) {
                    p.alert = alert;
                    cb(err, models,p);
                });
            },
        ], function(err, models, p) {
            res.data = p.alert;
            res.err = err;
            next();
        });

    }
}
