var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    request = require('request'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    config = require('annulet-config'),
    moment = require('moment');

module.exports = exports = {
    userOutstandingOPs: function(req, res, next) {
        logger.silly('checking user outstanding ops');
        async.waterfall([

            function(cb) {
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                projectUtility.instance.operatingProcedure.status(models, {
                    customerUser: req.actionCustomerUser,
                    actionCustomerUser: req.actionCustomerUser
                }, function(err, models, list) {
                    p.list = list;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //we're only interested in the incomplete ones
                p.list = _.filter(p.list, function(l) {
                    return !l.completedDate;
                });
                cb(null, models, p);
            }
        ], function(err, models, p) {
            res.err = err;
            res.data = p.list;
            next();
        });
    },
    userOutstandingWorkflows: function(req, res, next) {
        logger.silly('getting outstanding wfs...');
        async.waterfall([

            function(cb) {
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                logger.silly('[user outstanding wfs] models: ' + !!models);
                projectUtility.instance.workflow.status(models, {
                    actionCustomerUser: req.actionCustomerUser,
                    customerUser: req.actionCustomerUser,
                }, function(err, models, list) {
                    p.list = list;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //filter out completed wfs
                p.list = _.filter(p.list, function(l) {
                    return l.percentageComplete < 100;
                });
                cb(null, models, p);
            }
        ], function(err, models, p) {
            res.err = err;
            res.data = p.list;
            next();
        });
    },

    adminOutstandingOPs: function(req, res, next) {
        async.waterfall([

            function(cb) {
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                //todo: make sure customeruser is an admin
                cb(null, models, p);
            },
            function(models, p, cb) {
                //get the customer users for this customer
                projectUtility.auth.customerUsers(models, {
                    customer: req.customer,
                    actionCustomerUser: req.actionCustomerUser,
                    omitSystemUsers: true
                }, function(err, models, customerUsers) {
                    logger.silly('users: ' + util.inspect(customerUsers));
                    p.customerUsers = customerUsers;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //for each customer user, get user info for those customerusers and marry the data up
                async.parallel(_.map(p.customerUsers, function(customerUser) {
                    return function(cb) {
                        var url = config.configuration.paths.authUri() + '/user/get/' + customerUser.user.toString();
                        request.get({
                            url: url,
                            headers: {
                                'annulet-internal': config.configuration.paths.internalKey()
                            },
                            json: true
                        }, function(err, response, body) {
                            customerUser.user = body.data;
                            cb(err, body);
                        });
                    };
                }), function(err, r) {
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //for each customer user, get the wf status listing, filtering out completed listings
                async.parallel(_.map(p.customerUsers, function(customerUser) {
                    logger.silly('getting wf status listing for ' + util.inspect(customerUser));
                    return function(cb) {
                        projectUtility.instance.operatingProcedure.status(models, {
                            customerUser: customerUser,
                            actionCustomerUser: req.actionCustomerUser
                        }, function(err, models, list) {
                            customerUser.opStatusList = _.filter(list, function(l) {
                                return !l.completedDate;
                            });
                            cb(err, list);
                        });
                    };
                }), function(err, r) {
                    cb(err, models, p);
                });
            },
        ], function(err, models, p) {
            res.err = err;
            res.data = p.customerUsers;
            next();
        });
    },


    adminOutstandingWorkflows: function(req, res, next) {
        async.waterfall([

            function(cb) {
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                //todo: make sure customeruser is an admin
                cb(null, models, p);
            },
            function(models, p, cb) {
                //get the customer users for this customer
                projectUtility.auth.customerUsers(models, {
                    customer: req.customer,
                    actionCustomerUser: req.actionCustomerUser,
                    omitSystemUsers: true
                }, function(err, models, customerUsers) {
                    logger.silly('users: ' + util.inspect(customerUsers));
                    p.customerUsers = customerUsers;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //for each customer user, get user info for those customerusers and marry the data up
                async.parallel(_.map(p.customerUsers, function(customerUser) {
                    return function(cb) {
                        var url = config.configuration.paths.authUri() + '/user/get/' + customerUser.user.toString();
                        request.get({
                            url: url,
                            headers: {
                                'annulet-internal': config.configuration.paths.internalKey()
                            },
                            json: true
                        }, function(err, response, body) {
                            customerUser.user = body.data;
                            cb(err, body);
                        });
                    };
                }), function(err, r) {
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //for each customer user, get the wf status listing, filtering out completed listings
                async.parallel(_.map(p.customerUsers, function(customerUser) {
                    return function(cb) {
                        projectUtility.instance.workflow.status(models, {
                            customerUser: customerUser
                        }, function(err, models, list) {
                            customerUser.workflowStatusList = _.filter(list, function(l) {
                                return l.percentageComplete < 100;
                            });
                            cb(err, list);
                        });
                    };
                }), function(err, r) {
                    cb(err, models, p);
                });
            },
        ], function(err, models, p) {
            res.err = err;
            res.data = p.customerUsers;
            next();
        });
    },


    adminOPActivity: function(req, res, next) {
        async.waterfall([

            function(cb) {
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                //todo: make sure customeruser is an admin
                cb(null, models, p);
            },
            function(models, p, cb) {
                models.tracking.ItemActivity.find({
                    itemType: 'instances.operatingProcedure.OperatingProcedure'
                })
                    .sort('-modified')
                    .lean()
                    .exec(function(err, activities) {
                        p.opActivities = activities;
                        cb(err, models, p);
                    });
            },
        ], function(err, models, p) {
            res.err = err;
            res.data = p.opActivities;
            next();
        });
    },
    adminWorkflowActivity: function(req, res, next) {
        async.waterfall([

            function(cb) {
                cb(null, req.models, {});
            },
            function(models, p, cb) {
                //todo: make sure customeruser is an admin
                cb(null, models, p);
            },
            function(models, p, cb) {
                models.tracking.ItemActivity.find({
                    $or: [{
                        itemType: 'instances.quiz.Quiz'
                    }, {
                        itemType: 'instances.training.Training'
                    }]
                })
                    .sort('-modified')
                    .lean()
                    .exec(function(err, activities) {
                        p.trainingActivities = activities;
                        cb(err, models, p);
                    });
            },
        ], function(err, models, p) {
            res.err = err;
            res.data = p.trainingActivities;
            next();
        });
    },
};
