var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    moment = require('moment');
require('annulet-config');
module.exports = exports = {
    post: function(req, res) {
        logger.silly('got stripe hook');
        var eventJson = JSON.parse(req.body);
        logger.silly('customer: ' + eventJson.data.object.customer + ', event: ' + eventJson.type);
        //no matter what, send a 2xx back
        res.status(200).end();
    }
};
