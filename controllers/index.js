module.exports = exports = {
	account: require("./account"),
	admin: require("./admin"),
	alert: require("./alert"),
	auxiliaryDocument: require("./auxiliaryDocument"),
	complaint: require("./complaint"),
	customer: require("./customer"),
	errorSampler: require("./errorSampler"),
	health: require("./health"),
	navigationActivity: require("./navigationActivity"),
	operatingProcedure: require("./operatingProcedure"),
	problem: require("./problem"),
	quickView: require("./quickView"),
	quiz: require("./quiz"),
	signup: require("./signup"),
	stripeHook: require("./stripeHook"),
	training: require("./training"),
	workflow: require("./workflow"),
};
