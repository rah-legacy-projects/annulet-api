var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    configuration = require('annulet-config'),
    Mandrill = require('mandrill-api')
    .Mandrill,
    moment = require('moment');

module.exports = exports = {
    make: function(req, res) {
        logger.warn('[problem] detected');
        logger.warn('[problem] ' + util.inspect(req.body));
        async.waterfall([

            function(cb) {
                logger.silly('[problem] getting models');
                if (!req.models) {
                    if (/^[0-9a-fA-F]{24}$/.test(req.body.customer)) {
                        modeler.db({
                            collection: 'Customer',
                            query: {
                                customer: req.body.customer.toObjectId()
                            }
                        }, function(err, models) {
                            cb(err, models, {});
                        });
                    } else {
                        modeler.db(null, function(err, models) {
                            logger.warn('putting error on default server');
                            cb(err, models, {});
                        });
                    }
                } else {
                    cb(err, req.models, {});
                }
            },
            function(models, p, cb) {
                logger.silly('[problem] creating exception');
                (new models.tracking.Exception(req.body))
                    .save(function(err, exception) {
                        logger.silly('ticket number: ' + exception.ticketNumber);
                        p.exception = exception.toObject();
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                if (configuration.configuration.mandrill.sendExceptionEmail()) {
                    //enqueue email via mandrill
                    var mandrillClient = new Mandrill(configuration.configuration.mandrill.privateKey());

                    mandrillClient.messages.sendTemplate({
                        template_name: 'Exception',
                        template_content: {},
                        message: {
                            to: [{
                                email: 'support@cpandt.co',
                                name: 'Support @ CP&T',
                                type: 'to'
                            }],
                            subject: 'Ticket ' + p.exception.ticketNumber + ' in ' + p.exception.environment,
                            "merge_vars": [{
                                rcpt: 'support@cpandt.co',
                                vars: [{
                                    name: 'EXCEPTION',
                                    content: JSON.stringify(p.exception, null, 4)
                                }]
                            }]
                        }
                    }, function(result) {
                        logger.info('[problem] error sent via mandrill.');
                        cb(null, models, p);
                    }, function(error) {
                        logger.error('[problem] error sending email via mandrill: ' + util.inspect(error));
                        cb(null, models, p);
                    });
                } else {
                    logger.info('[problem] exception emails are turned off.');
                    cb(null, models, p);
                }
            },
        ], function(err, models, p) {
            logger.silly('[problem] finished');
            res.status(200)
                .json({
                    ticketNumber: p.exception.ticketNumber
                });
        });
    },
    coerce: function(req, res, next) {
        logger.warn('coercing error');
        res.statusOverride = 500;
        res.err = "this is a test error, coerced by " + req.loggedInUser.email;
        next();
    },
    get: function(req, res, next) {
        logger.silly('getting problem');
        async.waterfall([

            function(cb) {
                cb(err, req.models, {});
            },
            function(models, p, cb) {
                p._id = (function(str) {
                    var number = 0,
                        map = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    var istr = str.replace(/0/g, '');
                    _.each(istr.split('')
                        .reverse(), function(l, ix) {
                            number += (map.indexOf(l) + 1) * Math.pow(map.length, ix);
                        });
                    number = number - 1;
                    return number;
                })(req.params.ticket);
                cb(null, models, p);
            },
            function(models, p, cb) {
                logger.silly('[get problem] ticket: ' + req.params.ticket);
                models.tracking.Exception.find({
                    _id: p._id
                })
                    .exec(function(err, exception) {
                        logger.silly('[get problem] exception: ' + util.inspect(exception));
                        p.exception = exception;
                        cb(err, models, p);
                    });
            }
        ], function(err, models, p) {
            res.err = err;
            res.data = p.exception;
            next();
        });
    }
};
