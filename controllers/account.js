var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    request = require('request'),
    stripe = require('stripe')(require('annulet-config')
        .configuration.stripe.privateKey());
require('annulet-util');
module.exports = exports = {
    paymentDetail: function(req, res, next) {
        async.waterfall([

            function(cb) {
                cb(null, req.models);
            },
            function(models, cb) {
                //get the customer
                models.Customer.findOne({
                    _id: req.customer.toObjectId()
                })
                    .exec(function(err, customer) {
                        cb(err, models, {
                            customer: customer
                        });
                    });
            },
            function(models, p, cb) {
                //get the stripe customer
                stripe.customers.retrieve(p.customer.stripeCustomerId, function(err, stripeCustomer) {
                    p.stripeCustomer = stripeCustomer;
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            res.err = err;
            res.data = p;
            next();
        });
    },
    updateCustomer: function(req, res, next) {
        async.waterfall([

            function(cb) {
                cb(null, req.models);
            },
            function(models, cb) {
                //get the customer
                models.Customer.findOne({
                    _id: req.customer.toObjectId()
                })
                    .exec(function(err, customer) {
                        cb(err, models, {
                            customer: customer
                        });
                    });
            },
            function(models, p, cb) {
                //get the customer user
                models.auth.CustomerUser.findOne({
                    user: req.loggedInUser._id.toObjectId(),
                    customer: req.customer.toObjectId()
                })
                    .exec(function(err, customerUser) {
                        p.customerUser = customerUser;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                p.customer.name = req.body.name;
                p.customer.address.street1 = req.body.address.street1;
                p.customer.address.street2 = req.body.address.street2;
                p.customer.address.city = req.body.address.city;
                p.customer.address.state = req.body.address.state;
                p.customer.address.zip = req.body.address.zip;
                p.customer.phone = req.body.phone;
                p.customer.numberOfLocations = req.body.numberOfLocations;
                p.customer.numberOfEmployees = req.body.numberOfEmployees;
                p.customer.modifiedBy = p.customerUser._id.toString();
                p.customer.save(function(err, customer) {
                    p.customer = customer;
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            res.err = err;
            res.data = p.customer;;
            next();
        });
    },
    updatePayment: function(req, res, next) {
        async.waterfall([

            function(cb) {
                cb(null, req.models);
            },
            function(models, cb) {
                //get the customer
                models.Customer.findOne({
                    _id: req.customer.toObjectId()
                })
                    .exec(function(err, customer) {
                        cb(err, models, {
                            customer: customer
                        });
                    });
            },
            function(models, p, cb) {
                //get the customer user
                models.auth.CustomerUser.findOne({
                    user: req.loggedInUser._id.toObjectId(),
                    customer: req.customer.toObjectId()
                })
                    .exec(function(err, customerUser) {
                        p.customerUser = customerUser;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //get the stripe customer's cards
                //note that customers should only ever have one card on file, no need to iterate on has_more
                stripe.customers.listCards(p.customer.stripeCustomerId, function(err, cards) {
                    p.cards = cards.data;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //destroy the cards
                logger.silly('destroying cards for ' + p.customer.stripeCustomerId);
                async.parallel(_.map(p.cards, function(card) {
                    return function(cb) {
                        stripe.customers.deleteCard(p.customer.stripeCustomerId, card.id, function(err, confirmation) {
                            cb(err, confirmation);
                        });
                    };
                }), function(err, r) {
                    delete p.cards;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                logger.silly('creating card ' + req.body.token + ' on customer ' + p.customer.stripeCustomerId);
                //add the new card
                stripe.customers.createCard(p.customer.stripeCustomerId, {
                    source: req.body.token
                }, function(err, card) {
                    p.newCard = card;
                    cb(err, models, p);
                });
            },
        ], function(err, models, p) {
            if (!!err) {
                logger.error('problem saving card: ' + util.inspect(err));
            }
            res.err = err;
            res.data = p;
            next();
        });
    },
    deactivateAccount: function(req, res, next) {
        //mark customer as inactive
        //end stripe customer at the end of the period
        async.waterfall([

            function(cb) {
                cb(null, req.models);
            },
            function(models, cb) {
                //get the customer
                models.Customer.findOne({
                    _id: req.customer.toObjectId()
                })
                    .exec(function(err, customer) {
                        cb(err, models, {
                            customer: customer
                        });
                    });
            },
            function(models, p, cb) {
                //get the customer user
                models.auth.CustomerUser.findOne({
                    user: req.loggedInUser._id.toObjectId(),
                    customer: req.customer.toObjectId()
                })
                    .exec(function(err, customerUser) {
                        p.customerUser = customerUser;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //destroy subscription
                stripe.customers.cancelSubscription(p.customer.stripeCustomerId, p.subscription.id, {
                    at_period_end: true
                }, function(err, confirmation) {
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                p.modifiedBy = p.customerUser._id.toString();
                p.customer.save(function(err, customer) {
                    p.customer = customer;
                    cb(err, models, p)
                });
            },

        ], function(err, models, p) {
            res.customer = p.customer;
            res.err = err;
            next();
        });
    }
};
